const path = require('path');
const webpack = require("webpack");

const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devServer = {
    proxy: [
        {
            context: ['/public/rpc', '/api'],
            target: "http://0.0.0.0:9580",
            secure: false
        }
    ],
    historyApiFallback: {
        index: "/static/bundle/index.html"
    },
    compress: true,
    port: 9003
};


module.exports = env => {
    let {production = false} = env || {};

    return {
        devtool: production ? false : 'eval',
        devServer: devServer,
        entry: path.resolve(__dirname, "static/jsx/src/app.jsx"),
        output: {
            path: path.resolve(__dirname, "static/bundle/"),
            filename: "bundle.js",
            publicPath: '/static/bundle'
        },
        optimization: {
            splitChunks: {
                chunks: 'initial',
                filename: "vendor.js",
                cacheGroups: {
                    vendors: {
                        test: /node_modules\/(?!@kaiju.ui\/).*/,
                    }
                }
            },
            minimizer: !production ? [] : [
                new TerserPlugin({
                    parallel: true,
                    sourceMap: true,
                    terserOptions: {
                        ecma: 6,
                    },
                    extractComments: false
                }),
                new OptimizeCSSAssetsPlugin({})
            ],
            minimize: production
        },
        module: {
            rules: [
                {
                    test: /\.jsx$/,
                    exclude: /node_modules\/(?!@kaiju.ui\/).*/,
                    use: [
                        'thread-loader',
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ["@babel/env", "@babel/react"]
                            }
                        }
                    ],
                },
                {
                    test: /\.css$/,
                    include: [
                        /node_modules/,
                        path.resolve(__dirname, './static/style.bundle.css')
                    ],
                    loaders: [
                        !production
                            ? 'style-loader'
                            : MiniCssExtractPlugin.loader,
                        'css-loader']
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        !production
                            ? 'style-loader'
                            : MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                sassOptions: {
                                    includePaths: [
                                        path.resolve(__dirname, "node_modules/@kaiju.ui/components/src"),
                                        path.resolve(__dirname, "node_modules/@kaiju.ui/filters/src"),
                                        path.resolve(__dirname, "node_modules/@kaiju.ui/forms/src"),
                                        path.resolve(__dirname, "node_modules/@kaiju.ui/extra-forms/src"),
                                        path.resolve(__dirname, "node_modules/@kaiju.ui/app/src"),
                                        path.resolve(__dirname, "static/jsx/src")
                                    ]
                                }
                            }
                        },
                    ],
                },
                {
                    test: /\.less$/i,
                    include: /node_modules/,
                    use: [
                        {loader: 'style-loader'},
                        {loader: 'css-loader'},
                        {loader: 'less-loader'}]
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: 'file-loader'
                        }
                    ]
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
            alias: {
                'utils': path.resolve(__dirname, './static/jsx/src/utils'),
                'react': path.resolve('./node_modules/react'),
                'mobx': path.resolve('./node_modules/mobx'),
                'mobx-react': path.resolve('./node_modules/mobx-react'),
            },
        },
        plugins: [
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: '[name].css',
                chunkFilename: '[id].css',
            }),
            new webpack.ProvidePlugin({
                'utils': 'utils'
            }),
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, 'static/index.html'),
                hash: true
            }),
            // new CleanWebpackPlugin()
        ]
    };
};