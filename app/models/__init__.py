from kaiju_db.services import functions_registry

from .functions import *
from .tables import *

functions_registry.register_classes_from_namespace(globals())
