"""
Write your table definitions here.
"""

from datetime import datetime

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = (
    'metadata',
    'clients'
)

metadata = sa.MetaData()  #: user tables metadata

clients = sa.Table(
    'client', metadata,
    sa.Column(
        'id', sa_pg.UUID, primary_key=True,
        server_default=sa.text("uuid_generate_v4()")),
    sa.Column('username', sa.TEXT, unique=True, nullable=False),
    sa.Column('email', sa.TEXT, unique=True, nullable=False),
    sa.Column('password', sa_pg.BYTEA, nullable=False),
    sa.Column('salt', sa_pg.BYTEA, nullable=False),
    sa.Column(
        'created', sa.DateTime, nullable=False, default=datetime.utcnow,
        server_default=sa.func.timezone('UTC', sa.func.current_timestamp())),
    sa.Column(
        'settings', sa_pg.JSONB, nullable=False, default={},
        server_default=sa.text("'{}'::jsonb")),
    sa.Column(
        'private_info', sa_pg.JSONB, nullable=False, default={},
        server_default=sa.text("'{}'::jsonb")),
    sa.Column(
        'public_info', sa_pg.JSONB, nullable=False, default={},
        server_default=sa.text("'{}'::jsonb")),
)
sa.Index("idx_client_id", clients.c.id, postgresql_using="hash")
# sa.Index("idx_username", clients.c.username, postgresql_using="gin")