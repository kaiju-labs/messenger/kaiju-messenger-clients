from app.modules.validators import *


async def attribute_option_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("AttributeOption", "AttributeOption.is_exist",
                                  model_key=["attribute", "entity"])
    await _validator(app, key, value, ref)


async def attribute_option_not_exist_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("AttributeOption", "AttributeOption.is_not_exist",
                                  is_exists=False)
    await _validator(app, key, value, ref)
