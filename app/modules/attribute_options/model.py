from kaiju_model.model import BaseModel

from app.modules.validators import *
from app.modules.attribute_options.validators import *


class OptionKeyModel(BaseModel, abc.ABC):
    _name = StringField.type

    key = StringField()
    label = StringField()


class OptionLabelValue(BaseModel, abc.ABC):
    _name = StringField.type
    key = StringField()
    label = StringField()


class OptionCreateModel(BaseModel, abc.ABC):
    id = StringField(field_validator=attribute_option_exists_validator)
    attribute = StringField()
    entity = StringField(is_system=True)


class OptionEditModel(BaseModel, abc.ABC):
    id = StringField()
    attribute = StringField()
