from babel.dates import format_datetime
from kaiju_model.grid.base import BaseHandler


class NextRunHandler(BaseHandler):

    def call(self, values, **_):
        resp = {}
        for i in values:
            model = i["model"]

            if model.params["cron"]:
                resp[i["id"]] = format_datetime(i["value"], locale=self._locale or "ru_RU") if i["value"] else None

        return resp


class ProcessHandler(BaseHandler):

    async def call(self, values, **kws):
        resp = {}

        for v in values:
            _result = await self.app.services.CacheManager.get_task_state(v["model"].label, v["model"].engine)
            resp[v["id"]] = _result

        return resp
