from .common import SimpleLoadGridHandler


class RuleKindGridHandler(SimpleLoadGridHandler):
    _service = "RuleKind"


class RuleStatusGridHandler(SimpleLoadGridHandler):
    _service = "RuleStatus"