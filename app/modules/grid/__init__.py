from kaiju_model.grid.constructor import grid_handler_service

from . import common
from . import pages
from . import task
from . import media
from . import rule

grid_handler_service.register_classes_from_module(common)
grid_handler_service.register_classes_from_module(pages)
grid_handler_service.register_classes_from_module(task)
grid_handler_service.register_classes_from_module(media)
grid_handler_service.register_classes_from_module(rule)
