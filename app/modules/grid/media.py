from kaiju_model.grid.base import BaseHandler


class DocumentGridHandler(BaseHandler):
    async def call(self, values):

        files = []

        for i in values:
            if isinstance(i["value"], list):
                files.extend(i["value"])

            else:
                files.append(i["value"])

        files = list(set(files))

        files_data = await self.app.services.files.m_get(files)

        result = {}

        files_data = {str(_f["id"]): _f for _f in files_data}

        for i in values:
            id = str(i["id"])
            files_info = result.setdefault(id, [])

            if isinstance(i["value"], list):
                for v in i["value"]:
                    if v in files_data:
                        files_info.append({
                            "name": files_data[v]["name"],
                            "extension": files_data[v]["extension"],
                            "path": files_data[v]["uri"],
                        })

            elif i["value"] in files_data:
                v = i["value"]
                files_info.append({
                    "name": files_data[v]["name"],
                    "extension": files_data[v]["extension"],
                    "path": files_data[v]["uri"],
                })

        return result


class MediaDocumentGridHandler(DocumentGridHandler):

    async def _image_handler(self, values):
        id = [str(i["value"]) for i in values]
        photos = await self.app.services.images.get_gallery(id=id)
        photos = {
            str(i["id"]): {
                version: {
                    'id': data['correlation_id'],
                    'name': data['file']['name'],
                    'extension': data['file']['extension'],
                    'path': data['file']['uri']
                }
                for version, data in i['versions'].items()
            }
            for i in photos
        }
        result = {}

        for value in values:
            result[str(value["id"])] = photos.get(str(value["value"]), {})
        return result

    async def _document_handler(self, values):
        files = []

        for i in values:
            if isinstance(i["value"], list):
                files.extend(i["value"])

            else:
                files.append(i["value"])

        files = list(set(files))

        files_data = await self.app.services.files.m_get(files)

        result = {}

        files_data = {_f["id"]: _f for _f in files_data}

        for i in values:
            result.setdefault(i["id"], None)

            if isinstance(i["value"], list):
                for v in i["value"]:
                    if v in files_data:
                        result[i["id"]] = {
                            "name": files_data[v]["name"],
                            "extension": files_data[v]["extension"],
                            "path": files_data[v]["uri"],
                        }

            elif i["value"] in files_data:
                v = i["value"]
                result[i["id"]] = {
                    "name": files_data[v]["name"],
                    "extension": files_data[v]["extension"],
                    "path": files_data[v]["uri"],
                }

        return result

    async def call(self, values):
        entity = await self.app.services.Media.get_by_uuid(self._meta["entity"], grouping=False)

        if entity["settings"]["media_type"] == 'image':
            return await self._image_handler(values)

        return await self._document_handler(values)


class MediaItemSelectGridHandler(BaseHandler):
    async def call(self, values):
        id = []
        media_id = None
        for i in values:
            media_id = i["model"].params.get("media")
            value = i["value"]

            if value:
                id.extend(i["value"])

        if not id:
            return {}

        _data = {}

        if id and media_id:
            result = await self.app.services.MediaItem.grid(
                session=self._meta["session"],
                entity=media_id,
                uuid=id,
                system_locale=None,
                fields=["file"],
                per_page=len(id)
            )

            for _row in result["data"]:
                _value = _row["file"]["value"]

                if _value:
                    _data[_row["file"]["id"]] = _value

        _r = {}
        is_media_image = False

        for i in values:
            _new_value = []

            if i["value"]:
                for _v in i["value"]:
                    if not _v:
                        continue

                    if str(_v) in _data:
                        __v = _data[str(_v)]

                        if 'path' in __v:
                            _new_value.append(dict(__v))
                        elif 'original' in __v:
                            _new_value.append(__v["original"]["path"])
                            is_media_image = True
                        else:
                            self.logger.warning("Unpredictable media key %s" % str(__v))

            _r[i["id"]] = {
                'files': _new_value,
                'is_media_image': is_media_image
            }

        return _r
