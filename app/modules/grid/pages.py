# from babel.dates import format_datetime, format_date
from kaiju_model.grid.base import BaseHandler


__all__ = ["PageCacheStatusGridHandler"]


class PageCacheStatusGridHandler(BaseHandler):

    async def call(self, values):

        ids = [i["id"] for i in values]
        if not ids:
            return {}
        statuses =  await self.app.services.PageCaching.get_cache_info(ids)
        has_cache = await self.app.services.Router.router_list(ids)
        has_cache = {i["id"] for i in has_cache}
        r = {}
        for i in ids:
            r[i] = {
                **statuses.get(i, {}),
                "has_cache": i in has_cache
            }

        return r
