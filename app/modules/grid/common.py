from urllib.parse import urljoin

from babel.dates import format_datetime, format_date
from kaiju_model.grid.base import BaseHandler

__all__ = ["DateTimeGridHandler", "EntityKindGridHandler", "SimpleMultiSelectGridHandler", "SimpleLoadGridHandler"]

from kaiju_tools import ServerSessionFlag


def _format_date(value, locale):
    locale = locale or "ru_RU"
    return format_date(value, locale=locale) if value else None


def _format_datetime(value, locale):
    locale = locale or "ru_RU"
    return format_datetime(value, locale=locale) if value else None


class EntityKindGridHandler(BaseHandler):

    async def call(self, values):
        entity = self._meta["entity"]
        locale = self._locale
        id = [i["value"] for i in values if i["value"]]
        data = await self.app.services.Entity.load(system_locale=locale, parent=entity, id=id, per_page=len(id))
        data = {i["id"]: i["label"] for i in data["data"]}

        resp = {}
        for i in values:
            if i["value"] in data:
                resp[i["id"]] = f"""{data[i["value"]]} ({i["value"]})"""
        return resp


class ItemUUIDGridHandler(BaseHandler):

    async def call(self, values):
        resp = {}

        if values:
            locale = self._locale
            id = [i["value"] for i in values if i["value"]]
            entity = values[0]["model"].reference

            data = await self.app.services.Item.load_by_uuid(session=ServerSessionFlag, system_locale=locale,
                                                             entity=entity, id=id, per_page=len(id))

            data = {i["id"]: i["label"] for i in data["data"]}

            for i in values:
                if i["value"] in data:
                    resp[i["id"]] = f"""{data[i["value"]]}"""
        return resp


class DateGridHandler(BaseHandler):

    def call(self, values):
        _r = {}

        for i in values:
            behavior = getattr(i["model"], "behavior", None)
            value = i["value"]

            if not behavior:
                _r[i["id"]] = _format_date(value, self._meta)
                continue

            if isinstance(value, dict) and behavior == "range":
                _min = value.get("min")
                _max = value.get("max")

                if _min or _max:
                    _min = _format_date(_min, self._locale)
                    _max = _format_date(_max, self._locale)
                    _r[i["id"]] = f"""{_min or '..'} - {_max or '..'}"""
                else:
                    _r[i["id"]] = ""

            elif isinstance(value, list) and behavior == "list":
                _r[i["id"]] = "; ".join([_format_date(v, self._locale) for v in value])
            else:
                _r[i["id"]] = ""

        return _r


class DateTimeGridHandler(BaseHandler):

    def call(self, values):
        _r = {}

        for i in values:
            behavior = getattr(i["model"], "behavior", None)
            value = i["value"]

            if not behavior:
                _r[i["id"]] = _format_datetime(value, self._locale)
                continue

            if isinstance(value, dict) and behavior == "range":
                _min = value.get("min")
                _max = value.get("max")

                if _min or _max:
                    _min = _format_datetime(_min, self._locale) if _min else None
                    _max = _format_datetime(_max, self._locale) if _max else None
                    _r[i["id"]] = f"""{_min or '..'} - {_max or '..'}"""
                else:
                    _r[i["id"]] = ""

            elif isinstance(value, list) and behavior == "list":
                _r[i["id"]] = "; ".join([_format_datetime(v, self._locale) for v in value])
            else:
                _r[i["id"]] = ""

        return _r


class SimpleLabelHandler(BaseHandler):

    def call(self, values):
        _r = {}
        for _row in values:
            _r[_row["id"]] = _row["value"] or f"""[{_row["id"]}]"""

        return _r


class PhotoGridHandler(BaseHandler):

    async def call(self, values, **kwargs):
        app_host = self.app.settings["etc"]["app_host"]
        _resp = {}
        if values:
            model = values[0]["model"]
            is_single = model.params.get("single", False)
            id = []
            for i in values:
                if i["value"]:
                    if is_single:
                        id.append(i["value"][0])
                    else:
                        id.extend(i["value"])

            photos = await self.app.services.images.get_gallery(id=id)
            photo_map = {}
            for i in photos:
                photo_map[i["id"]] = urljoin(app_host, i['versions']["original"]["file"]['uri'])

            for i in values:
                if i["value"]:
                    if is_single:
                        _val = photo_map.get(i["value"][0])
                    else:
                        _val = []
                        for _v in i["value"]:
                            if _v in photo_map:
                                _val.append(photo_map[_v])
                    if _val:
                        _resp[i["id"]] = _val
        return _resp


class CategoryGridHandler(BaseHandler):
    async def call(self, values):
        cat_ids = []
        for value in values:
            if value["value"]:
                cat_ids.extend(value["value"])

        if not cat_ids:
            return {}

        categories_data = await self.app.services.CategoryItem.load(id=cat_ids, system_locale=self._locale, tree=True,
                                                                    session=ServerSessionFlag, get_base_id=False)
        categories = categories_data["data"]

        if not categories:
            pass

        categories_map = {}
        for category in categories:
            categories_map[category["id"]] = category

        result = {}
        for _data in values:
            if not _data:
                continue

            _cat_ids = _data["value"]

            if not _cat_ids:
                continue

            _mapped_value = []
            for _cat_id in _cat_ids:

                _cat_map = categories_map.get(_cat_id)

                if _cat_map:
                    _mapped_value.append(_cat_map)

            result[_data['id']] = _mapped_value

        return result


class SimpleLoadGridHandler(BaseHandler):
    _service = None

    async def call(self, values):
        _values = set([i["value"] for i in values])

        data = await getattr(self.app.services, self._service).load(
            entity=self._meta.get("entity"), system_locale=self._locale, id=list(_values), per_page=None)
        data = {i["id"]: i["label"] for i in data["data"]}

        _r = {}
        for i in values:
            _r[i["id"]] = data.get(i["value"], i["value"])

        return _r


class SimpleMultiSelectGridHandler(BaseHandler):
    async def call(self, values):
        ids = []
        for value in values:
            if value["value"]:
                ids.extend(value["value"])

        if not ids:
            return {}

        data = await getattr(self.app.services, self._service).load(id=ids, system_locale=self._locale,
                                                                    session=ServerSessionFlag, get_base_id=False)
        data_values = data["data"]

        if not values:
            pass

        data_values_map = {}
        for data_value in data_values:
            data_values_map[data_value["id"]] = data_value

        result = {}
        for _data in values:
            if not _data:
                continue

            _ids = _data["value"]

            if not _ids:
                continue

            _mapped_value = []
            for _id in _ids:

                _cat_map = data_values_map.get(_id)

                if _cat_map:
                    _mapped_value.append(_cat_map["label"])

            result[_data['id']] = ", ".join(_mapped_value)

        return result


class PrototypeGridHandler(SimpleLoadGridHandler):
    _service = "Prototype"
