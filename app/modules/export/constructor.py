from kaiju_model.grid.constructor import ProductGridConstructor


class ItemsExportConstructor(ProductGridConstructor):

    @staticmethod
    def _get_handler(attr):
        handler = getattr(attr, "export_handler", None)
        return handler

    # async def init(self):
    #     results = await self._call_handlers()
    #
    #     for _row in self._order:
    #         print(">>", _)
    #         _attributes = _row["attributes"]
    #
    #         for _attr_object in _attributes:
    #             _key = _attr_object["attribute"]
    #             _model = self._attributes[_key]
    #
    #             if _model.params.get("lookup"):
    #                 _attr_object["lookup"] = _model.params["lookup"]
    #
    #             if _model.params.get("media"):
    #                 _attr_object["media"] = _model.params["media"]
    #
    #             _handler = self._get_handler(_model)
    #
    #             if _handler and _handler in self._grid_handler_service:
    #                 _value = results.get(_key, {}).get(_row["id"])
    #                 _attr_object["value"] = _value
    #
    #         self._grid.append(_row)
