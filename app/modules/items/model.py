import abc

from kaiju_model.fields import StringField, SelectField
from kaiju_model.model import BaseModel

from .fields import ItemIdField
from .validators import item_exists_validator


class ItemCreateModel(BaseModel, abc.ABC):
    id = ItemIdField(
        # field_validator=item_exists_validator,
        required=True)
    entity = StringField(is_system=True)


class ItemKindCreateModel(BaseModel, abc.ABC):
    id = ItemIdField(
        # field_validator=item_exists_validator,
        required=True)
    prototype = SelectField(options_handler="Prototype.load", required=True)
    entity = StringField(is_system=True)
