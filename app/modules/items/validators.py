from app.modules.validators import *


async def item_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Item", "Item.is_exist", model_key="entity")
    await _validator(app, key, value, ref)


async def item_not_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Item", "Item.is_not_exist", model_key="entity", is_exists=False)
    await _validator(app, key, value, ref)
