from functools import partial

from kaiju_model.fields import _f_empty_value, StringField

ItemIdField = partial(
    StringField,
    read_only=True,
    required=True,
    validator=_f_empty_value,
    regex=(r"^[A-Za-z0-9_-]+$", "Regex.key")
)
