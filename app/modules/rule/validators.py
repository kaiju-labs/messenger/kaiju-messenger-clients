from app.modules.validators import *


async def rule_exists_validator(app, key: str, value, ref=None):
    await RaiseIfExists(
        service_name="Rule",
        error_code="Rule.is_exist",
        complex_key=True,
        model_key="entity"
    )(app, key, value, ref)