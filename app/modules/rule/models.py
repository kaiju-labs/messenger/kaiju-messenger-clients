from kaiju_model.model import BaseModel

from app.modules.grid.rule import RuleKindGridHandler, RuleStatusGridHandler
from app.modules.grid.common import DateTimeGridHandler
from .validators import *

__all__ = ('RuleGridModel', 'RuleCreateModel', 'RuleEditModel', 'RuleUpdateEnabledModel', 'RuleUpdatePriorityModel')


class RuleCreateModel(BaseModel, abc.ABC):
    id = SimpleIdField(field_validator=rule_exists_validator, required=True)
    label = StringField(required=True)
    kind = SelectField(read_only=True, required=True, options_handler="RuleKind.load")
    priority = IntegerField(required=True, negative_value=False)
    entity = StringField(required=True, is_system=True)


class RuleGridModel(BaseModel, abc.ABC):
    id = StringField()
    label = StringField()
    status = SelectField(grid_handler=RuleStatusGridHandler.__name__)
    enabled = BooleanField()
    kind = SelectField(grid_handler=RuleKindGridHandler.__name__)
    priority = IntegerField(negative_value=False)
    updated = DateTimeField(grid_handler=DateTimeGridHandler.__name__)
    created = DateTimeField(grid_handler=DateTimeGridHandler.__name__)
    execute_task = StringField(is_system=True,
                               # TODO:
                               # grid_handler=RuleTasksGridHandler.__name__
                               )


class RuleUpdateEnabledModel(BaseModel, abc.ABC):
    enabled = BooleanField(default=False)


class RuleUpdatePriorityModel(BaseModel, abc.ABC):
    priority = IntegerField(negative_value=False, default=1)


class RuleEditModel(BaseModel, abc.ABC):
    entity = StringField(required=True, is_system=True)
    status = SelectField(required=True, options_handler="RuleStatus.load", read_only=True)
    label = StringField(required=True)
    enabled = BooleanField(required=True)  # TODO: validate
    kind = SelectField(required=True, read_only=True, options_handler="RuleKind.load")
    priority = IntegerField(required=True, negative_value=False)
    # actions = JSONObjectField()
    # conditions = JSONObjectField()
