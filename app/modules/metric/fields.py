from decimal import Decimal

from kaiju_model.fields import BaseField


class Unit(BaseField):
    _name = "Metric.unit"

    def __init__(self, convert_to, convert_from, *args,  **kwargs):
        """
        :param convert_to: unit
        :param convert_from: unit to base
        """
        super(Unit, self).__init__(*args, **kwargs)
        self.convert_to = convert_to
        self.convert_from = convert_from

    def normalize(self, value):
        value = Decimal(str(value))

        if callable(self.convert_to):
            return self.convert_to(value)

        return value * Decimal(self.convert_to)
