import abc
from decimal import Decimal

from .base import BaseMetric
from .fields import Unit

__all__ = [
    'MetricAreaDensity',
    'MetricDen',
    'MetricLength',
    'MetricLuminousFlux',
    'MetricMass',
    'MetricMassFlowRate',
    'MetricTemperature',
    'MetricTime',
    'MetricUnits',
    'MetricVolume',
    'MetricPower'
]


class MetricLength(BaseMetric, abc.ABC):
    _attr_name_ = 'length'

    micrometer = Unit(convert_from=1000000, convert_to=Decimal("0.000001"))
    millimeter = Unit(convert_from=1000, convert_to=Decimal("0.001"))
    centimeter = Unit(convert_from=100, convert_to=Decimal("0.01"))
    decimeter = Unit(convert_from=10, convert_to=Decimal("0.1"))
    meter = Unit(convert_from=1, convert_to=1)
    kilometer = Unit(convert_from=Decimal("0.001"), convert_to=1000)

    inch = Unit(convert_from=Decimal("39.3701"), convert_to=Decimal("0.0254"))


class MetricMass(BaseMetric, abc.ABC):
    _attr_name_ = 'mass'

    milligram = Unit(convert_from=1000000, convert_to=Decimal("0.000001"))

    gram = Unit(convert_from=1, convert_to=1)
    kilogram = Unit(convert_from=Decimal("0.001"), convert_to=1000)
    ton = Unit(convert_from=0.000001, convert_to=1000000)


class MetricUnits(BaseMetric, abc.ABC):
    _attr_name_ = 'units'

    unit = Unit(convert_from=1, convert_to=1)


class MetricVolume(BaseMetric, abc.ABC):
    _attr_name_ = 'volume'

    millilitre = Unit(convert_from=1000, convert_to=Decimal("0.001"))
    litre = Unit(convert_from=1, convert_to=1)
    cubic_meter = Unit(convert_from=Decimal("0.001"), convert_to=1000)


class MetricDen(BaseMetric, abc.ABC):
    _attr_name_ = 'den'

    den = Unit(convert_from=1, convert_to=1)


class MetricAreaDensity(BaseMetric, abc.ABC):
    _attr_name_ = 'area_density'
    gram_per_sq_meter = Unit(convert_from=1, convert_to=1)

    kilogram_per_sq_meter = Unit(convert_from=Decimal("0.001"), convert_to=1000)


class MetricPower(BaseMetric, abc.ABC):
    _attr_name_ = 'power'
    watt = Unit(convert_from=1, convert_to=1)
    kilowatt = Unit(convert_from=Decimal("0.001"), convert_to=1000)


class MetricLuminousFlux(BaseMetric, abc.ABC):
    _attr_name_ = 'luminous_power'
    lumen = Unit(convert_from=1, convert_to=1)


def fahrenheit_to_celsius(value: float):
    val = (value - Decimal(32)) * Decimal(5) / Decimal(9)
    return val


def celsius_to_fahrenheit(value: float):
    val = value * Decimal(9) / Decimal(5) + Decimal(32)
    return val


class MetricTemperature(BaseMetric, abc.ABC):
    _attr_name_ = 'temperature'

    celsius = Unit(convert_from=1, convert_to=1)
    fahrenheit = Unit(
        convert_from=celsius_to_fahrenheit,
        convert_to=fahrenheit_to_celsius
    )


class MetricMassFlowRate(BaseMetric, abc.ABC):
    _attr_name_ = 'mass_flow_rate'
    gram_per_hour = Unit(convert_from=1, convert_to=1)
    kilogram_per_hour = Unit(convert_from=Decimal("0.001"), convert_to=1000)


class MetricVolumetricFlowRate(BaseMetric, abc.ABC):
    _attr_name_ = 'volumetric_flow_rate'
    cubic_meter_per_second = Unit(convert_from=1, convert_to=1)
    litre_per_minute = Unit(convert_from=Decimal("0.00001667"), convert_to=60000)


class MetricTime(BaseMetric, abc.ABC):
    _attr_name_ = 'time'
    minute = Unit(convert_from=1, convert_to=1)
    hour = Unit(convert_from=Decimal("0.0166667"), convert_to=60)
    second = Unit(convert_from=60, convert_to=Decimal("0.0166667"))
