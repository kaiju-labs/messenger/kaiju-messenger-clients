import abc

from kaiju_model.model import BaseModel


class BaseMetric(BaseModel, abc.ABC):

    def convert(self, _from, _to, _value):
        """
        >> .convert(MetricLength.millimeter, MetricLength.kilometer, 1000)
        :param _from:
        :param _to:
        :param _value:
        :return:
        """
        return

    @classmethod
    def normalize_value(cls, unit, value):
        if not unit or not value:
            return None

        _unit = getattr(cls, unit, None)

        if _unit:
            return _unit.normalize(value)

    @classmethod
    def get_fields(cls):
        return {f"{name}": {"id": f"{name}"} for name in cls._base_fields}

