import inspect

from . import family
from .base import BaseMetric
from app.services.metric import MetricFamilyService


MetricFamiliesHash = {}
MetricFamilies = {}


for name, cls in tuple(family.__dict__.items()):
    if inspect.isclass(cls):
        if issubclass(cls, BaseMetric) and BaseMetric in cls.__bases__:
            MetricFamiliesHash[MetricFamilyService.generate_uuid(cls._attr_name_)] = cls
            MetricFamilies[cls._attr_name_] = cls
