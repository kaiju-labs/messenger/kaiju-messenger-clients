from kaiju_model.model import BaseModel

from app.modules.validators import *

__all__ = ['GridSearchSettings', 'ItemSearchSettings', 'CategorySearchSettings']


class GridSearchSettings(BaseModel, abc.ABC):
    engine = SelectField(options_handler="SearchClient.load_engines")
    categories = StringField(behavior="list")
    pre_filters = SelectField(options_handler="SearchClient.load_prefilters", dependence={"key": "engine"})
    sort = SelectField(options_handler="SearchClient.load_sort", dependence={"key": "engine"})
    filters = JSONObjectField(behavior="dict")


class ListSearchSettings(BaseModel, abc.ABC):
    engine = SelectField(options_handler="SearchClient.load_engines")
    categories = StringField(behavior="list")
    pre_filters = SelectField(options_handler="SearchClient.load_prefilters", dependence={"key": "engine"})
    # weights = SelectField(options_handler="SearchClient.load_weights", dependence={"key": "engine"})
    sort = SelectField(options_handler="SearchClient.load_sort", dependence={"key": "engine"})
    filters = JSONObjectField(behavior="dict")


class CategorySearchSettings(BaseModel, abc.ABC):
    engine = SelectField(options_handler="SearchClient.load_engines")
    categories = StringField(behavior="list")
    depth = IntegerField(default=3)


class ItemSearchSettings(BaseModel, abc.ABC):
    engine = SelectField(options_handler="SearchClient.load_engines")


SEARCH_SETTINGS_MODELS = {
    "product_grid": GridSearchSettings,
    "product_list": ListSearchSettings,
    "categories_grid": CategorySearchSettings,
    "one_product": ItemSearchSettings
}
