import abc

from kaiju_model.fields import BooleanField, IntegerField, StringField, SimpleIdField
from kaiju_model.model import BaseModel

from .validators import attribute_group_exists_validator, attribute_group_not_exist_validator
from ..fields import LabelField

__all__ = ['AttributeGroupCreateModel', 'AttributeGroupEditModel', 'AttributeGroupGridModel']


class AttributeGroupCreateModel(BaseModel, abc.ABC):
    id = SimpleIdField(field_validator=attribute_group_exists_validator)
    label = StringField()
    sort = IntegerField(required=False, negative_value=False, is_system=True)
    is_default = BooleanField(required=False, is_system=True)
    subject = StringField(is_system=True)
    entity = StringField(is_system=True)


class AttributeGroupGridModel(BaseModel, abc.ABC):
    uuid = SimpleIdField()
    id = SimpleIdField()
    label = LabelField()
    sort = IntegerField()


class AttributeGroupEditModel(BaseModel, abc.ABC):
    id = SimpleIdField(field_validator=attribute_group_not_exist_validator)
    sort = IntegerField(required=False, negative_value=False, is_system=True)
    is_default = BooleanField(required=False, is_system=True)
    entity = StringField(is_system=True)


