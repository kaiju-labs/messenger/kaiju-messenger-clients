from app.modules.validators import *


async def attribute_group_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("AttributeGroup", "AttributeGroup.is_exist", model_key="entity")
    await _validator(app, key, value, ref)


async def attribute_group_not_exist_validator(app, key: str, value, ref=None):
    pass
    # _validator = ServiceValidator("AttributeGroup", "AttributeGroup.is_not_exist", model_key="entity",
    #                               is_exists=False)
    # await _validator(app, key, value, ref)
