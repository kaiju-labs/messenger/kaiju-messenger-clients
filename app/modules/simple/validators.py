from app.modules.validators import *


async def entity_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Entity", "Entity.is_exist", model_key="subject")
    await _validator(app, key, value, ref)


async def entity_not_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Entity", "Entity.is_exist", is_exists=False, model_key="subject")
    await _validator(app, key, value, ref)