import abc

from kaiju_model.fields import SimpleIdField, StringField, ObjectField
from kaiju_model.model import BaseModel

from ..fields import LabelField
from .validators import entity_exists_validator

__all__ = ['EntityCreateModel', 'EntityEditModel', 'EntityGridModel', 'EntityGetOrCreateModel']


class EntityGridModel(BaseModel, abc.ABC):
    uuid = SimpleIdField()
    id = SimpleIdField()
    label = LabelField()


class EntityCreateModel(BaseModel, abc.ABC):
    id = SimpleIdField(field_validator=entity_exists_validator, required=True)
    labels = ObjectField(default={})

    label = LabelField()
    subject = StringField(is_system=True)


class EntityGetOrCreateModel(BaseModel, abc.ABC):
    id = SimpleIdField(required=True)
    label = LabelField()
    subject = StringField(is_system=True)


class EntityEditModel(BaseModel, abc.ABC):
    id = SimpleIdField()
