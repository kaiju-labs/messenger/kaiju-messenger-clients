from functools import partial

from kaiju_model.fields import *
from kaiju_tools.exceptions import ValidationError


class ServiceValidator:
    def __init__(self, service_name, error_code, is_exists=True, service_key=None, model_key=None,  complex_key=False,):
        """
        Валидация осуществляется через service.get сервиса переданного в service_name

        :param service_name:
        :param error_code:
        :param is_exists:
        :param service_key: Дополнительный параметр из сервиса на передачу в service.get
        :param model_key: Дополнительный параметр из модели на передачу в service.get
        """
        self.service_name = service_name
        self.error_code = error_code
        self.is_exists = is_exists
        self.service_key = service_key
        self.model_key = model_key
        self.complex_key = complex_key

    async def __call__(self, app, key, value, ref=None, **kwargs):
        _kwargs = {}
        _service = getattr(app.services, self.service_name)

        if self.model_key and type(self.model_key) is str:
            _kwargs[self.model_key] = ref[self.model_key]
        elif self.model_key and type(self.model_key) is list:
            for _key in self.model_key:
                _kwargs[_key] = ref[_key]

        if self.service_key:
            _kwargs[self.service_key] = getattr(_service, self.service_key)

        try:
            if not self.complex_key:
                await _service.get(value, **_kwargs)
            else:
                test= await _service.get({"id": value, **_kwargs})
        except Exception:
            if not self.is_exists:
                raise ValidationError('Invalid id', data=dict(key=key, value=value, code=self.error_code))
        else:
            if self.is_exists:
                raise ValidationError('Invalid id', data=dict(key=key, value=value, code=self.error_code))


RaiseIfExists = partial(ServiceValidator, is_exists=True)
RaiseIfNotExists = partial(ServiceValidator, is_exists=False)


def _f_normalizer_example(value):
    """Normalization function example.

    :param value:
    :return: normalized value if possible
    :raises should rise a `app.modules.fields.NormalizationError` in case of the problem
    """


def _f_validator_example(key: str, value, ref, **__):
    """Validation function interface example.

    :param key: attribute name
    :param value: attribute value
    :param ref: reference (conditional) value
    :raises ValidationError: should rise a validation error
    :return: return value is not used, sorry
    """


async def _f_async_validator_example(app, key: str, value, ref, **__):
    """Validation function interface example.

    :param app: web application object will be passed automatically to the function
    :param key: attribute name
    :param value: attribute value
    :param ref: reference (conditional) value
    :raises ValidationError: should rise a validation error
    :return: return value is not used, sorry
    """


def _f_min_value(key: str, value, ref, **__):
    if value < ref:
        raise ValidationError('Value is too small.',
                              data=dict(key=key, value=value, code='ValidationError.ValueTooSmall'))


def _f_max_value(key: str, value, ref, **__):
    if value > ref:
        raise ValidationError('Value is too big.', data=dict(key=key, value=value, code='ValidationError.ValueTooBig'))


def _f_negative_value(key: str, value, ref, **__):
    if (value < 0) and not ref:
        raise ValidationError('Negative value is not allowed.',
                              data=dict(key=key, value=value, code='ValidationError.ValueNegative'))


def _f_max_length(key: str, value, ref, **__):
    if not isinstance(value, (dict, list, str, tuple)):
        value = str(value)

    if len(value) > ref:
        raise ValidationError('Value is too long.',
                              data=dict(key=key, value=value[:ref], code='ValidationError.ValueTooLong'))


def _f_max_items(key: str, value, ref, **__):
    if isinstance(value, dict):
        if len(value) > ref:
            raise ValidationError(f'Dict gets too many items. Max: {ref}, given: {len(value)}',
                                  data=dict(key=key, value=value, code='ValidationError.TooManyItems'))
    if isinstance(value, list):
        if len(value) > ref:
            raise ValidationError(f'List gets too many items. Max: {ref}, given: {len(value)}',
                                  data=dict(key=key, value=value, code='ValidationError.TooManyItems'))


def _f_min_length(key: str, value, ref, **__):
    if not isinstance(value, (dict, list, str, tuple)):
        value = str(value)

    if len(value) < ref:
        raise ValidationError('Value is too short.',
                              data=dict(key=key, value=value, code='ValidationError.ValueTooShort'))


def _f_empty_value(key: str, value, ref, **__):
    if not value and not ref:
        raise ValidationError('Value must not be empty or null.',
                              data=dict(key=key, value=value, code='ValidationError.ValueEmpty'))


def _f_sort_validator(key: str, value, ref, **__):
    pass
    # if value is not None and value < 1:
    #     raise ValidationError('Value must be positive.', key=key, value=value, code='ValueNegative')


async def group_validator(app, key: str, value, ref, **__):
    pass
    # if value:
    #     result = await app.services.CatalogueClient.call(
    #         method="AttributeGroup.get",
    #         params={
    #             "id": value,
    #             "get_system": True
    #         }
    #     )
    #
    #     groups = result["groups"]
    #     if not groups or groups[0]["key"] != value:
    #         raise ValidationError('Invalid group', data=dict(key=key, value=value, code='AttributeGroup.invalid'))


def behavior_validator(app, key: str, value, ref=None, **__):
    if key not in ["list", "range", None]:
        raise ValidationError('Invalid behaviour', data=dict(key=key, value=value, code="ValidationError.invalid"))


def string_behavior_validator(app, key: str, value, ref=None, **__):
    if key not in ["list", None]:
        raise ValidationError('Invalid behaviour', data=dict(key=key, value=value, code="ValidationError.invalid"))


async def metric_family_validator(app, key: str, value, ref, **__):
    if not app.services.MetricOption.validate_family(value):
        raise ValidationError(f'Invalid family "{value}"', data=dict(key=key, value=value, code='MetricFamily.invalid'))


def json_object_behavior_validator(app, key: str, value, ref=None, **__):
    if key not in ["list", "dict"]:
        raise ValidationError(f'Invalid behaviour {key}. Behavior must be "list" or "dict".',
                              data=dict(key=key, value=value, code="ValidationError.invalid"))


async def currency_family_validator(app, key: str, value, ref, **__):
    pass


async def attribute_keys_validator(app, key: str, value, ref, **__):
    pass
    # result = await app.services.CatalogueClient.call(
    #     method="Attribute.get",
    #     params={
    #         "key": value,
    #         "get_system": False
    #     }
    # )
    #
    # if result['count'] == len(value):
    #     return
    #
    # value = set(value)
    # keys = set(attr["key"] for attr in result["attributes"])
    # missing_attrs = list(value.difference(keys))
    #
    # raise ValidationError(f'Attributes not exists: {",".join(missing_attrs)}',
    #                       key=key, value=value, code='ProductAttribute.is_not_exist')


async def product_attribute_unique_validator(app, key, value, ref, model=None, **kwargs):
    locale = getattr(model, 'locale', None)
    id = getattr(model, 'product_id')
    channel_id = getattr(model, 'channel_id', None)
    per_channel = getattr(model, 'per_channel')
    per_locale = getattr(model, 'per_locale')

    # response = await app.services.CatalogueClient.call(
    #     method="Product.check_unique_value",
    #     params={
    #         "key": key,
    #         "value": value,
    #         "id": id,
    #         "channel_id": channel_id if per_channel else None,
    #         "locale": locale if per_locale else None
    #     }
    # )
    #
    # if response.get("available") is not None and not response.get("available"):
    #     raise ValidationError(f'Not unique {key}: {value}', key=key, value=value, code='ProductAttribute.is_exist')


KeyField = partial(
    StringField,
    read_only=True,
    required=True,
    validator=_f_empty_value,
    regex=(r"^[a-zA-Z0-9_]+$", "Regex.key")
)

UseInGrid = partial(BooleanField, default=True, group=FieldGroups.SEARCH)
IsRich = partial(BooleanField, default=False)

UseInFiltering = partial(BooleanField, default=False, group=FieldGroups.SEARCH, grid_handler="FilterHandler")
UseInSearch = partial(BooleanField, default=False, group=FieldGroups.SEARCH, grid_handler="SearchHandler")

EmptyValue = partial(BooleanField, default=True, validator=_f_empty_value, group=FieldGroups.CONDITIONS)
NegativeValue = partial(BooleanField, default=True, validator=_f_negative_value, group=FieldGroups.CONDITIONS)
DecimalValue = partial(BooleanField, default=True, group=FieldGroups.CONDITIONS)

MinIntValue = partial(IntegerField, negative_value=True, validator=_f_min_value, group=FieldGroups.CONDITIONS)
MaxIntValue = partial(IntegerField, negative_value=True, validator=_f_max_value, group=FieldGroups.CONDITIONS)

MinDecimalValue = partial(DecimalField, negative_value=True, validator=_f_min_value, group=FieldGroups.CONDITIONS)
MaxDecimalValue = partial(DecimalField, negative_value=True, validator=_f_max_value, group=FieldGroups.CONDITIONS)

MinDateValue = partial(DateField, validator=_f_min_value, group=FieldGroups.CONDITIONS)
MaxDateValue = partial(DateField, validator=_f_max_value, group=FieldGroups.CONDITIONS)

MinDateTimeValue = partial(DateTimeField, validator=_f_min_value, group=FieldGroups.CONDITIONS)
MaxDateTimeValue = partial(DateTimeField, validator=_f_max_value, group=FieldGroups.CONDITIONS)

MinLength = partial(IntegerField, validator=_f_min_length, negative_value=True, group=FieldGroups.CONDITIONS)
MaxLength = partial(IntegerField, validator=_f_max_length, negative_value=True, group=FieldGroups.CONDITIONS)

MinCharacters = partial(IntegerField, validator=_f_min_length, negative_value=False, group=FieldGroups.CONDITIONS)
MaxCharacters = partial(IntegerField, validator=_f_max_length, negative_value=False, group=FieldGroups.CONDITIONS)

MaxItems = partial(IntegerField, validator=_f_max_items, negative_value=False, group=FieldGroups.CONDITIONS)

Sort = partial(IntegerField, field_validator=_f_sort_validator, negative_value=False, group=FieldGroups.BASE)

Behavior = partial(SelectField,
                   required=False,
                   read_only=True,
                   options_handler="AttributeBehavior.load",
                   field_validator=behavior_validator,
                   )

StringBehaviour = partial(SelectField,
                          required=False,
                          read_only=True,
                          options_handler="AttributeBehavior.string_load",
                          field_validator=behavior_validator,
                          )

JSONObjectBehaviour = partial(SelectField,
                              required=True,
                              read_only=True,
                              options_handler="AttributeBehavior.json_object_load",
                              field_validator=json_object_behavior_validator,
                              )


async def exists_type_validator(app, key: str, value, ref, method, id_key="id", code="ValidationError.is_exist",
                                extra_keys: list = None):
    params = {
        id_key: value
    }

    # if extra_keys:
    #     for _key in extra_keys:
    #         params[_key] = ref[_key]
    #
    # responses = await app.services.CatalogueClient.call(method=method, params=params)
    #
    # count = responses["count"]
    #
    # if count > 0:
    #     raise ValidationError(f'Object exists', data=dict(key=key, value=value, code=code))


async def not_exists_type_validator(app, key: str, value, ref, method, id_key="id",
                                    code="ValidationError.is_not_exist", extra_keys: list = None):
    params = {
        id_key: value
    }

    # if extra_keys:
    #     for _key in extra_keys:
    #         params[_key] = ref[_key]
    #
    # responses = await app.services.CatalogueClient.call(method=method, params={id_key: value})
    # count = responses["count"]
    #
    # if count > 0:
    #     raise ValidationError(f'Object not exists', data=dict(key=key, value=value, code=code))


async def identical_attribute_kinds(app, key: str, value, ref, **__):
    attribute_to = await app.services.EntityClient.call(
        method="Attribute.get",
        params={
            "subject": "product",
            "id": value
        }
    )
    attribute_to_kind = attribute_to["kind"]

    attribute_from = await app.services.EntityClient.call(
        method="Attribute.get",
        params={
            "subject": "product",
            "id": ref["attribute_from"]
        }
    )
    attribute_from_kind = attribute_from["kind"]

    if attribute_to_kind != attribute_from_kind:
        raise ValidationError("Kinds of attributes are not the same",
                              data=dict(key=key, value=value, code='ValidationError.KindsNotIdentical'))
