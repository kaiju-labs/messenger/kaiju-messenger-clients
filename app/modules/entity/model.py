import abc

from kaiju_model.fields import SimpleIdField, StringField, MultiselectField, ObjectField, SelectField, BooleanField
from kaiju_model.model import BaseModel

from ..fields import LabelField
from .validators import entity_exists_validator
from app.modules.attribute.grid_handlers import ProjectionHandler

__all__ = ['EntityCreateModel', 'EntityEditModel', 'EntityGridModel', 'EntityGetOrCreateModel']


class EntityGridModel(BaseModel, abc.ABC):
    uuid = SimpleIdField()
    id = SimpleIdField()
    label = LabelField()
    projection = MultiselectField(
        disabled=False,
        required=False,
        default=[],
        read_only=False,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )


class EntityCreateModel(BaseModel, abc.ABC):
    uuid = StringField(is_system=True, required=False)
    subject = StringField(is_system=True, required=True)
    labels = ObjectField(default={})

    id = SimpleIdField(field_validator=entity_exists_validator, required=True)
    label = LabelField()

    projection = MultiselectField(
        disabled=False,
        required=False,
        default=[],
        read_only=False,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    main_projection = SelectField(
        disabled=False,
        required=False,
        default=None,
        read_only=False,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__,
        nested="settings"
    )
    category = MultiselectField(
        required=False,
        default=[],
        options_handler="CategoryItem.load",
        nested="settings",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    enable_rules = BooleanField(default=False, nested="settings")


class EntityGetOrCreateModel(BaseModel, abc.ABC):
    id = SimpleIdField(required=True)
    label = LabelField()
    subject = StringField(is_system=True)


class EntityEditModel(BaseModel, abc.ABC):
    id = SimpleIdField()
    projection = MultiselectField(
        disabled=True,
        required=False,
        default=[],
        read_only=True,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    category = MultiselectField(
        required=False,
        default=[],
        options_handler="CategoryItem.load",
        nested="settings",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    main_projection = SelectField(
        disabled=True,
        required=False,
        default=None,
        read_only=True,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__,
        nested="settings"

    )
    enable_rules = BooleanField(default=False, nested="settings")

