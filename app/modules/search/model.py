import abc

from kaiju_model.model import BaseModel
from kaiju_model.fields import DateTimeField, StringField, SelectField, BooleanField, JSONObjectField, MultiselectField

from ..fields import LabelField

__all__ = ['SearchCreateModel', 'SearchViewModel']


class SearchCreateModel(BaseModel, abc.ABC):
    label = LabelField(required=True)
    entity = SelectField(options_handler="Entity.load", nested="settings", required=True)
    channel = SelectField(options_handler="Entity.load", nested="settings", required=False)
    category = SelectField(options_handler="Entity.load", nested="settings", required=False)
    language = SelectField(options_handler="Search.language_load", nested="settings", required=True)
    is_simple = BooleanField(default=True, nested="settings")

    correlations = JSONObjectField(is_system=True, nested="settings")

    def to_dict(self):
        correlations = []

        for k in ("entity", "channel", "category"):
            val = self.params.get(k)
            if val:
                correlations.append(val)

        self.params["correlations"] = correlations

        return super().to_dict()


class SearchViewModel(BaseModel, abc.ABC):
    id = StringField()
    label = LabelField()
    entity = SelectField(nested="settings")
    is_active = BooleanField()
    settings = StringField()
    updated = DateTimeField()


class SearchEditModel(BaseModel, abc.ABC):
    label = LabelField(required=True)
    is_active = BooleanField()

    entity = SelectField(options_handler="Entity.load", nested="settings", read_only=True)
    channel = SelectField(options_handler="Entity.load", nested="settings", read_only=True)
    category = SelectField(options_handler="Entity.load", nested="settings")
    language = SelectField(options_handler="Search.language_load", nested="settings", read_only=True)
    reference_as_object = MultiselectField(options_handler="Search.reference_loads", nested="settings")
    is_simple = BooleanField(read_only=True, nested="settings")
    correlations = JSONObjectField(is_system=True, nested="settings")

    def to_dict(self):
        correlations = []

        for k in ("entity", "channel", "category"):
            val = self.params.get(k)
            if val:
                correlations.append(val)

        self.params["correlations"] = correlations

        return super().to_dict()


class SearchUpdateModel(BaseModel, abc.ABC):
    label = LabelField()
    is_active = BooleanField(nested="settings")
    category = SelectField(nested="settings")
