import abc

from kaiju_model.fields import StringField, BooleanField, DateTimeField, IntegerField, JSONObjectField
from kaiju_model.model import BaseModel

from ..grid.task import NextRunHandler, ProcessHandler


class TaskViewModel(BaseModel, abc.ABC):
    id = StringField()
    label = StringField()
    description = StringField()
    cron = StringField()
    status = StringField()
    active = BooleanField()
    status_change = DateTimeField()
    process = StringField(grid_handler=ProcessHandler.__name__)
    next_run = DateTimeField(grid_handler=NextRunHandler.__name__)
    exit_code = IntegerField()
    result = JSONObjectField()
