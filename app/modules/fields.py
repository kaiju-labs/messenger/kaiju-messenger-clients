from functools import partial

from kaiju_model.fields import StringField

LabelField = partial(
    StringField,
    grid_handler="SimpleLabelHandler"
)
