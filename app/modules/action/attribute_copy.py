import abc

from kaiju_model.fields import SelectField, JSONObjectField
from kaiju_model.model import BaseModel

from app.modules.attribute.validators import attribute_not_exist_validator
# from app.modules.channel.validators import channel_not_exist_validator
# from app.modules.locale.validators import locale_not_exist_validator
# from app.modules.validators import template_not_exist_validator
# from app.modules.variant.validators import variant_not_exist_validator
from .validators import copy_action_attributes_validators


class AttributeCopyActionModel(BaseModel, abc.ABC):
    class Main(BaseModel, abc.ABC):
        attribute_from = SelectField(options_handler="EntityAttribute.load", group="Group.rule_from")
        projection_from = JSONObjectField(is_system=True)

    class Extra(BaseModel, abc.ABC):
        attribute_to = SelectField(options_handler="EntityAttribute.load", group="Group.rule_to")
        projection_to = JSONObjectField(is_system=True)


class AttributeCopyActionEditModel(BaseModel, abc.ABC):
    attribute_from = SelectField(
        options_handler="EntityAttribute.load",
        required=True
        # field_validator=attribute_not_exist_validator
    )
    attribute_to = SelectField(
        options_handler="EntityAttribute.load", required=True,
        # field_validator=copy_action_attributes_validators
    )
    projection_from = JSONObjectField(required=False)
    projection_to = JSONObjectField(required=False)
