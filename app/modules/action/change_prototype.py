import abc

from kaiju_model.fields import SelectField
from kaiju_model.model import BaseModel


# from app.modules.channel.validators import channel_not_exist_validator
# from app.modules.locale.validators import locale_not_exist_validator
# from app.modules.validators import template_not_exist_validator
# from app.modules.variant.validators import variant_not_exist_validator


class PrototypeActionModel(BaseModel, abc.ABC):
    template = SelectField(options_handler="Prototype.load", group="Group.rule_actions")


class PrototypeActionEditModel(BaseModel, abc.ABC):
    template = SelectField(options_handler="Prototype.load", required=True,
                           # field_validator=template_not_exist_validator
                           )
