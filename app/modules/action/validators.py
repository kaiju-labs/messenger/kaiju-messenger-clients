from kaiju_tools.exceptions import ValidationError

from app.modules.attribute.validators import attribute_not_exist_validator, identical_attribute_kinds, kind_validator, \
    behavior_validator


async def snippet_logic_validator(app, key: str, value, ref=None):
    list_action = ref.get("list_action")
    attributes = ref.get("attributes")

    if app.services.RuleSnippetAction.include_snippet(list_action) and not attributes:
        raise ValidationError('No attributes with include logic',
                              data=dict(key=key, value=value, code='RuleSnippet.no_attributes'))


async def copy_action_attributes_validators(app, key: str, value, ref=None):
    """
    Проверка attribute_from и attribute_to на одинаковый каинд
    И атрибут attribute_from на существование

    :param app:
    :param key:
    :param value:
    :param ref:
    :return:
    """
    await attribute_not_exist_validator(app=app, key=key, value=value, ref=ref)
    await identical_attribute_kinds(app=app, key=key, ref=ref, value=value)


async def snippet_json_attr_validators(app, key: str, value, ref=None):
    """
    :param app:
    :param key:
    :param value:
    :param ref:
    :return:
    """
    await attribute_not_exist_validator(app=app, key=key, value=value, ref=ref)
    await kind_validator(app=app, key=key, value=value, kind="json_object")
    await behavior_validator(app=app, key=key, value=value, behavior="list")
