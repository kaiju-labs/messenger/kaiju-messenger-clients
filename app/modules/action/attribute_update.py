import abc

from kaiju_model.fields import SelectField, ObjectField, MultiselectField, StringField
from kaiju_model.model import BaseModel

from app.modules.attribute.validators import attribute_not_exist_validator
# from app.modules.channel.validators import channel_not_exist_validator
# from app.modules.locale.validators import locale_not_exist_validator
# from app.modules.validators import template_not_exist_validator
# from app.modules.variant.validators import variant_not_exist_validator
from .validators import copy_action_attributes_validators, snippet_json_attr_validators, snippet_logic_validator


class AttributesChooseField(MultiselectField):

    @staticmethod
    def normalize(value, **_):
        """
        Нормализирует словарь значений атрибутов из базы в список ключей.

        :param value:
        :param _:
        :return:
        """
        if not value:
            return []

        return list(value.keys())


class AttributeUpdateActionModel(BaseModel, abc.ABC):
    """
    Первая часть формы выбора атрибутов. Нужна для выбора ключей атрибутов,
    которые будут редактрироваться.
    """
    attributes = AttributesChooseField(options_handler="EntityAttribute.load", group="Group.rule_actions")


class AttributeUpdateActionEditModel(BaseModel, abc.ABC):
    attributes = ObjectField(is_system=True, required=True)
