import abc

from kaiju_model.fields import SelectField
from kaiju_model.model import BaseModel


class CategoryAddActionModel(BaseModel, abc.ABC):
    action = SelectField(options_handler="RuleAction.load", group="Group.rule_actions", default="set", read_only=True)
    value = SelectField(is_system=True)


class CategoryAddActionEditModel(BaseModel, abc.ABC):
    action = SelectField(options_handler="RuleAction.load", required=True, default="set", read_only=True)
    value = SelectField(is_system=True, required=True)


class CategoryMoveActionModel(BaseModel, abc.ABC):
    action = SelectField(options_handler="RuleAction.load", group="Group.rule_actions", default="move", read_only=True)
    value = SelectField(is_system=True)


class CategoryMoveActionEditModel(BaseModel, abc.ABC):
    action = SelectField(options_handler="RuleAction.load", required=True, default="move", read_only=True)
    value = SelectField(is_system=True, required=True)


class CategoryDeleteActionModel(BaseModel, abc.ABC):
    action = SelectField(options_handler="RuleAction.load", group="Group.rule_actions", default="delete",
                         read_only=True)
    value = SelectField(is_system=True)


class CategoryDeleteActionEditModel(BaseModel, abc.ABC):
    action = SelectField(options_handler="RuleAction.load", required=True, default="delete", read_only=True)
    value = SelectField(is_system=True, required=True)
