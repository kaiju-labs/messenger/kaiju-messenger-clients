import abc

from kaiju_model.fields import SelectField, ObjectField, MultiselectField, StringField
from kaiju_model.model import BaseModel

from app.modules.attribute.validators import attribute_not_exist_validator
# from app.modules.channel.validators import channel_not_exist_validator
# from app.modules.locale.validators import locale_not_exist_validator
# from app.modules.validators import template_not_exist_validator
# from app.modules.variant.validators import variant_not_exist_validator
from .validators import copy_action_attributes_validators, snippet_json_attr_validators, snippet_logic_validator

__all__ = (
    'ProductVariantActionModel',
    'ProductVariantActionEditModel',
    'ProductSnippetActionModel',
    'ProductSnippetActionEditModel',
    'ProductVariantActionEditModel',
)



class SKUtoNewModelActionModel:
    template = SelectField(options_handler="Template.load")
    variant = SelectField(options_handler="Variant.load")


class SKUtoNewModelActionEditModel:
    template = SelectField(
        required=True, options_handler="Template.load",
        # field_validator=template_not_exist_validator
    )
    variant = SelectField(
        required=True, options_handler="Variant.load",
        # field_validator=variant_not_exist_validator
    )


class SKUtoModelActionModel:
    model = SelectField(options_handler="Template.load")


class SKUtoModelActionEditModel:
    model = SelectField(required=True)


class ProductVariantActionModel:
    class Main(BaseModel, abc.ABC):
        template = SelectField(options_handler="Template.load")

    class Extra(BaseModel, abc.ABC):
        template = StringField(read_only=True, is_system=True)
        variant = SelectField(options_handler="Variant.load")
        attributes = MultiselectField(options_handler="ProductAttribute.load")

        def form_normalize(self):
            self.params["params"] = {
                "template": self.params["template"]
            }


class ProductVariantActionEditModel(BaseModel, abc.ABC):
    template = SelectField(
        read_only=True, required=True,
        # field_validator=template_not_exist_validator
    )
    attributes = MultiselectField()
    variant = SelectField(
        required=True,
        # field_validator=variant_not_exist_validator,
        options_handler="Variant.load"
    )

    def form_normalize(self):
        self.params["params"] = {
            "template": self.params.get("template")
        }


class ProductSnippetActionModel(BaseModel, abc.ABC):
    json_attribute = SelectField(
        options_handler="ProductAttribute.load",
        options_params={
            "kind": "json_object",
            "settings": {
                "behavior": "list"
            }
        },
        group="Group.rule_actions"
    )
    # TODO: включим потом
    # grouping = BooleanField(group="Group.rule_actions")
    list_action = SelectField(group="Group.rule_actions", options_handler="RuleSnippetAction.load",
                              default="snippet_include")


class ProductSnippetActionEditModel(BaseModel, abc.ABC):
    # TODO: attribute is exists
    attributes = ObjectField(is_system=True)
    json_attribute = SelectField(options_handler="ProductAttribute.load", required=True,
                                 field_validator=snippet_json_attr_validators)
    # grouping = BooleanField()
    list_action = SelectField(group="Group.rule_actions", options_handler="RuleSnippetAction.load",
                              default="snippet_include", field_validator=snippet_logic_validator)