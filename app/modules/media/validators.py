from app.modules.validators import *


async def media_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Media", "Media.is_exist")
    await _validator(app, key, value, ref)


async def media_not_exist_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Media", "Media.is_not_exist", is_exists=False)
    await _validator(app, key, value, ref)
