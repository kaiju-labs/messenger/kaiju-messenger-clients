from kaiju_model.model import BaseModel

# from app.modules.grid.media.handlers import MediaTypeGridHandler
from app.modules.validators import *
from .validators import media_exists_validator, media_not_exist_validator

__all__ = ('MediaCreateModel', 'MediaEditModel', 'MediaGridModel', 'VideoMediaItemModel', 'ImageMediaItemModel',
           'DocumentMediaItemModel')

from ..attribute.grid_handlers import ProjectionHandler
from ..fields import LabelField


class MediaCreateModel(BaseModel, abc.ABC):
    uuid = StringField(is_system=True, required=False)
    subject = StringField(is_system=True, required=True)
    labels = ObjectField(default={})

    id = SimpleIdField(field_validator=media_exists_validator, required=True)
    label = LabelField()
    media_type = SelectField(options_handler="MediaType.load", nested="settings")
    unique_only = BooleanField(default=False, nested="settings")
    projection = MultiselectField(
        disabled=False,
        required=False,
        default=[],
        read_only=False,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )


class MediaGridModel(BaseModel, abc.ABC):
    uuid = SimpleIdField()
    id = SimpleIdField()
    label = LabelField()
    projection = MultiselectField(
        disabled=False,
        required=False,
        default=[],
        read_only=False,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    media_type = SelectField(options_handler="MediaType.load",
                             # grid_handler=MediaTypeGridHandler.__name__
                             nested="settings")
    unique_only = BooleanField(default=False, nested="settings")


class MediaEditModel(BaseModel, abc.ABC):
    id = SimpleIdField()
    media_type = SelectField(read_only=True, options_handler="MediaType.load", nested="meta")
    unique_only = BooleanField(default=False, read_only=True, nested="settings")
    projection = MultiselectField(
        disabled=True,
        required=False,
        default=[],
        read_only=True,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    # category = MultiselectField(
    #     required=False,
    #     default=[],
    #     options_handler="CategoryItem.load",
    #     nested="settings",
    #     # field_validator=group_validator,
    #     grid_handler=ProjectionHandler.__name__
    # )


class DocumentMediaItemModel(BaseModel, abc.ABC):
    prototype = SelectField(options_handler="Prototype.load", required=True)
    files = DocumentField(required=True, max_number_of_files=1000)


class VideoMediaItemModel(BaseModel, abc.ABC):
    prototype = SelectField(options_handler="Prototype.load", required=True)
    files = VideoField(required=True, max_number_of_files=1000)


class ImageMediaItemModel(BaseModel, abc.ABC):
    prototype = SelectField(options_handler="Prototype.load", required=True)
    files = PhotoField(required=True, max_number_of_files=1000)
