import abc

from kaiju_model.fields import SimpleIdField, StringField, MultiselectField, ObjectField, SelectField, BooleanField
from kaiju_model.model import BaseModel

from ..fields import LabelField
from .validators import entity_exists_validator
from app.modules.attribute.grid_handlers import ProjectionHandler

__all__ = ['SiteCreateModel', 'SiteEditModel', 'SiteGridModel']


class SiteGridModel(BaseModel, abc.ABC):
    uuid = SimpleIdField()
    id = SimpleIdField()
    label = LabelField()
    projection = MultiselectField(
        disabled=False,
        required=False,
        default=[],
        read_only=False,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    channel = SelectField(
        disabled=False,
        required=False,
        default=None,
        read_only=False,
        options_handler="Projection.load_by_uuid",
        nested="settings",
        grid_handler=ProjectionHandler.__name__
    )


class SiteCreateModel(BaseModel, abc.ABC):
    uuid = StringField(is_system=True, required=False)
    subject = StringField(is_system=True, required=True)
    labels = ObjectField(default={})

    id = SimpleIdField(field_validator=entity_exists_validator, required=True)
    label = LabelField()

    projection = MultiselectField(
        disabled=False,
        required=False,
        default=[],
        read_only=False,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )

    channel = SelectField(
        disabled=False,
        required=False,
        default=None,
        read_only=False,
        options_handler="Projection.load_by_uuid",
        nested="settings",
        grid_handler=ProjectionHandler.__name__
    )


# class EntityGetOrCreateModel(BaseModel, abc.ABC):
#     id = SimpleIdField(required=True)
#     label = LabelField()
#     subject = StringField(is_system=True)


class SiteEditModel(BaseModel, abc.ABC):
    id = SimpleIdField()
    projection = MultiselectField(
        disabled=True,
        required=False,
        default=[],
        read_only=True,
        options_handler="Projection.load",
        # field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )
    channel = SelectField(
        disabled=True,
        required=False,
        default=None,
        read_only=True,
        options_handler="Projection.load_by_uuid",
        nested="settings",
        grid_handler=ProjectionHandler.__name__
    )
