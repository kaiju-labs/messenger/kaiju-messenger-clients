from decimal import Decimal
from numbers import Number


from kaiju_model.model import BaseModel as Default

from app.modules.validators import *
from ..metric import MetricFamiliesHash

MIN_LONG = - 1.7e+38
MAX_LONG = 1.7e+38
# MIN_LONG = - 2 ** 38
# MAX_LONG = (2 ** 38) - 2
# MIN_DATE = 0
# MAX_DATE = (2 ** 38) - 2


class BaseEsNormalize:
    @staticmethod
    def normalize_es(value):
        return value


class ESNumericNormalize:
    def normalize_es(self, value):
        # behavior = self.behavior
        # negative_value = getattr(self, "negative_value", None)
        #
        # if behavior == 'range' and type(value) is dict:
        #     _min_val = value.get("min", None)
        #     _max_val = value.get("max", None)
        #
        #     if _min_val is None or _min_val == '':
        #         if negative_value is None and negative_value:
        #             _min_val = MIN_LONG
        #         else:
        #             _min_val = 0
        #     else:
        #         if _min_val < MIN_LONG:
        #             _min_val = MIN_LONG
        #         elif _min_val > MAX_LONG:
        #             _min_val = MAX_LONG
        #
        #     if _max_val is None or _max_val == '':
        #         _max_val = MAX_LONG
        #     else:
        #         if _max_val < MIN_LONG:
        #             _max_val = MIN_LONG
        #         elif _max_val > MAX_LONG:
        #             _max_val = MAX_LONG
        #
        #     return [_min_val, _max_val]
        # else:
        try:
            return Decimal(value)
        except:
            return None
        # return Decimal(value) if value != '' else None


class BaseModel(Default, abc.ABC):

    async def serialize(self, value, **_):
        return value, set()


class BooleanModel(BaseModel):
    __value__ = BooleanField()

    is_unique = None
    use_in_grid = UseInGrid(default=True, nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    grid_handler = StringField(default="BooleanLoadGridHandler", is_system=True, nested="settings")
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    async def serialize(self, value, **_):
        return bool(value), set()

    @staticmethod
    def normalize_es(value):
        if type(value) is bool:
            return int(value)


class StringModel(BaseModel, BaseEsNormalize):
    __value__ = StringField()

    use_in_grid = UseInGrid(nested="settings")
    min_characters = MinCharacters(nested="settings")
    max_characters = MaxCharacters(nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    use_in_search = UseInSearch(default=False)
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="string")

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error:
            if (value and not value.strip()) or not value:
                return None, error

        return value, error


class StringListModel(BaseModel, BaseEsNormalize):
    class Field(StringField):
        _name = "string_list"

    __value__ = Field()
    is_unique = None
    use_in_grid = UseInGrid(nested="settings")
    min_characters = MinCharacters(nested="settings")
    max_characters = MaxCharacters(nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    use_in_search = None
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="string_list")
    grid_handler = StringField(default="ListGridHandler", is_system=True, nested="settings")
    behavior = StringField(is_system=True, default="list", nested="settings")

    async def validate(self, value, *_, **__):
        _errors = []
        _vals = []

        if type(value) is list:

            for i in value:
                _value, error = await self._validate(i)

                if error:
                    _errors.extend(error)
                _vals.append(_value)

        return _vals, _errors or None


class LinkField(StringField):
    _name = "link"


class LinkModel(BaseModel):
    __value__ = LinkField()

    use_in_grid = UseInGrid(nested="settings")
    min_characters = MinCharacters(nested="settings")
    max_characters = MaxCharacters(nested="settings")
    read_only = BooleanField(default=False, nested="settings")

    # behavior = StringBehaviour(nested="settings")


class KeyModel(StringModel):
    __value__ = BaseKeyField()
    # read_only = BooleanField(disabled=True, read_only=True, is_system=True, default=False, nested="settings")
    # use_in_grid = UseInGrid(default=True, read_only=True, nested="settings")
    #
    # use_in_filtering = UseInFiltering(read_only=True, default=True)
    # use_in_search = None
    # min_characters = None
    # max_characters = None
    # group = SelectField(
    #     read_only=True,
    #     disabled=True,
    #     required=False,
    #     default=False,
    #     options_handler="AttributeGroup.load",
    #     field_validator=group_validator,
    #     grid_handler='AttributeGroupHandler'
    # )


class IDModel(StringModel):
    class IDField(BaseKeyField):
        _name = "id"

    __value__ = IDField()
    read_only = BooleanField(disabled=True, read_only=True, is_system=True, default=False, nested="settings")
    use_in_grid = UseInGrid(default=True, read_only=True, nested="settings")

    use_in_filtering = UseInFiltering(read_only=True, default=True)
    use_in_search = None
    min_characters = None
    max_characters = None
    group = SelectField(
        read_only=True,
        disabled=True,
        required=False,
        default=False,
        options_handler="AttributeGroup.load",
        field_validator=group_validator,
        grid_handler='AttributeGroupHandler'
    )



class TextModel(BaseEsNormalize, BaseModel):
    __value__ = TextField()

    use_in_grid = UseInGrid(default=False, nested="settings")
    is_rich = IsRich(nested="settings")
    max_characters = MaxLength(nested="settings")
    grid_handler = StringField(default="TextGridHandler", is_system=True, nested="settings")
    use_in_search = UseInSearch(default=False)
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="text")

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error:
            if (value and not value.strip()) or not value:
                return None, error

        return value, error


class IntegerModel(ESNumericNormalize, BaseModel):
    __value__ = IntegerField()
    # behavior = Behavior(nested="settings")

    use_in_filtering = UseInFiltering(default=False)
    use_in_grid = UseInGrid(nested="settings")

    negative_value = NegativeValue(nested="settings")
    min_value = MinIntValue(nested="settings")
    max_value = MaxIntValue(nested="settings")
    grid_handler = StringField(default="BaseNumberGridHandler", is_system=True, nested="settings")
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error:
            if not value and not isinstance(value, Number):
                return None, error

        return value, error


class DecimalModel(ESNumericNormalize, BaseModel):
    __value__ = DecimalField()

    negative_value = NegativeValue(nested="settings")
    min_value = MinDecimalValue(nested="settings")
    max_value = MaxDecimalValue(nested="settings")

    use_in_filtering = UseInFiltering(default=False)
    use_in_grid = UseInGrid(nested="settings")
    # behavior = Behavior(nested="settings")
    grid_handler = StringField(default="BaseNumberGridHandler", is_system=True, nested="settings")
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error:
            if not value and not isinstance(value, Number):
                return None, error

        return value, error


class DateModel(BaseModel):
    __value__ = DateField()

    is_unique = None
    use_in_grid = UseInGrid(nested="settings")
    min_value = MinDateValue(nested="settings")
    max_value = MaxDateValue(nested="settings")
    use_in_filtering = UseInFiltering(default=False)

    grid_handler = StringField(default="DateGridHandler", is_system=True, nested="settings")
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    def normalize_es(self, value):
        if value:
            return Decimal(value.strftime("%s"))

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error:
            if not value:
                return None, error

        return value, error


class DateTimeModel(DateModel):
    __value__ = DateTimeField()
    __date_format = "%Y-%m-%dT%H:%M:%S"

    is_unique = None
    use_in_grid = UseInGrid(nested="settings")
    min_value = MinDateTimeValue(nested="settings")
    max_value = MaxDateTimeValue(nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    prototype = StringField(is_system=True, default="numeric")

    enable_time = BooleanField(default=True, is_system=True, nested="settings")

    grid_handler = StringField(default="DateTimeGridHandler", is_system=True, nested="settings")
    read_only = BooleanField(default=False, nested="settings")

    def normalize_es(self, value):
        if value:
            return value.timestamp()

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class SelectModel(BaseEsNormalize, BaseModel):
    __value__ = SelectField()

    is_unique = None
    use_in_grid = UseInGrid(nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    # use_in_search = UseInSearch(default=False, nested="settings")

    has_options = BooleanField(default=True, is_system=True, nested="settings")
    options_handler = StringField(default="AttributeOption.load", is_system=True, nested="settings")

    # TODO добавить проверку хендлера
    grid_handler = StringField(default="ProductAttributeOptionGridHandler", is_system=True, nested="settings")
    read_only = BooleanField(default=False, nested="settings")

    prototype = StringField(is_system=True, default="string_list")

    # def form_normalize(self):
    #     self.params["params"] = {
    #         "attribute": self.params["uuid"]
    #     }

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class MultiSelectAttributeModel(SelectModel):
    __value__ = MultiselectField()
    is_unique = None
    grid_handler = StringField(default="ProductAttributeMultiOptionGridHandler", is_system=True, nested="settings")

    def _is_required(self, value):
        return not self.params.get("required") and not value

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class ReferenceMultiselectAttributeModel(BaseEsNormalize, BaseModel):
    class ReferenceMultiselectField(SelectField):
        _name = "reference_multiselect"

    __value__ = ReferenceMultiselectField()

    is_unique = None

    # TODO добавить валидатор
    reference = SelectField(
        required=True,
        read_only=True,
        options_handler="Entity.load_by_uuid",
        group='Group.entity'
    )
    default = MultiselectField(
        required=False,
        is_system=True,
        options_handler="Item.load",
        group='Group.entity',
        nested="settings"
    )
    # behavior = StringField(is_system=True, default="list", nested="settings")

    options_handler = StringField(default="Item.load", is_system=True, nested="settings")
    use_in_grid = UseInGrid(nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    grid_handler = StringField(
        # TODO: default="LookupItemMultiSelectGridHandler",

        is_system=True, nested="settings")

    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="string_list")

    def _is_required(self, value):
        return not self.params.get("required") and not value

    def form_normalize(self):
        beh_val = self.params.get("behavior")
        if not beh_val:
            behavior = self._fields.get("behavior")
            if behavior:
                beh_val = behavior.default or None

        self.params["params"] = {
            # "behavior": beh_val,
            "entity": self.params.get("reference")
        }
        self.params["behavior"] = beh_val

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error

    def modify(self, name, field, **_):
        if name == 'default':
            if self.params.get("reference"):
                return {"params": {"entity": self.params.get("reference")}, "is_system": False}
        return {}

    async def serialize(self, value, current_level, max_levels, channel=None, serialize_service: object=None,  **_):
        if serialize_service is None:
            serialize_service = self.app.services.Item

        default = self.params.get("default")
        if not value:
            value = default

        dependency_key = set()
        if current_level > max_levels:
            return value, dependency_key
        if not value:
            return value, dependency_key

        reference = self.params["reference"]

        if isinstance(value, (list, tuple)):
            resp = []
            for _v in value:

                result, nested_dependency_key = await serialize_service.serialize(
                    channel=channel,
                    id=_v, entity=reference, current_level=current_level, max_levels=max_levels, )

                # todo: add channel
                dependency_key.add(f"{reference}#{_v}")
                dependency_key.update(nested_dependency_key)

                if result:
                    resp.append(result)

            return resp, dependency_key

        result, nested_dependency_key = await serialize_service.serialize(
            id=value, channel=channel, entity=reference, current_level=current_level, max_levels=max_levels)

        dependency_key.add(f"{reference}#{value}")
        dependency_key.update(nested_dependency_key)
        return result, dependency_key


class ReferenceListAttributeModel(ReferenceMultiselectAttributeModel):
    class ReferenceListField(SelectField):
        _name = "reference_list"

    __value__ = ReferenceListField()

    behavior = StringField(is_system=True, default="list", nested="settings")
    prototype = StringField(is_system=True, default="string_list")

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class ReferenceSelectAttributeModel(ReferenceMultiselectAttributeModel):
    class ReferenceSelectField(SelectField):
        _name = "reference_select"

        @staticmethod
        def normalize(value, **_):
            return value

    __value__ = ReferenceSelectField()

    default = SelectField(
        required=False,
        is_system=True,
        options_handler="Item.load",
        group='Group.entity',
        nested="settings"
    )
    behavior = None
    is_unique = None
    grid_handler = StringField(
        # TODO: default="LookupItemSelectGridHandler",
        is_system=True, nested="settings")
    prototype = StringField(is_system=True, default="string_list")

    def _is_required(self, value):
        return not self.params.get("required") and value in {'', None}

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class MediaAttributeModel(BaseEsNormalize, BaseModel):
    __value__ = MediaField()

    is_unique = None

    # TODO добавить валидатор
    media = SelectField(
        required=True,
        read_only=True,
        options_handler="Media.load_by_uuid",
        group="Group.media",
        nested="settings"
    )

    options_handler = StringField(default="MediaItem.load", is_system=True, nested="settings")
    use_in_grid = UseInGrid(nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    read_only = BooleanField(default=False, nested="settings")
    grid_handler = StringField(default="MediaItemSelectGridHandler", is_system=True, nested="settings")
    export_handler = StringField(default="MediaItemSelectGridHandler", is_system=True, nested="settings")
    prototype = StringField(is_system=True, default="counter_list")

    def _is_required(self, value):
        return not self.params.get("required") and not value

    def form_normalize(self):
        self.params["params"] = {
            "entity": self.params.get("media")
        }

    async def validate(self, value, *_, **__):
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class JSONObjectModel(BaseModel):
    __value__ = JSONObjectField()

    max_items = MaxItems(nested="settings")
    use_in_grid = UseInGrid(nested="settings")
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="json")

    async def validate(self, value: Any, **_) -> Tuple[Any, List[ValidationError]]:
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error

    @staticmethod
    def normalize_es(*_, **__):
        return None


class DocumentModel(BaseModel):
    __value__ = DocumentField()

    is_unique = None
    use_in_grid = None
    single = BooleanField(group=FieldGroups.BASE, nested="settings")
    max_size_mb = IntegerField(negative_value=False, group=FieldGroups.CONDITIONS, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    def _is_required(self, value):
        return not self.params.get("required") and not value

    def normalize_es(self, value):
        if type(value) is list:
            return len(value)

    async def serialize(self, value, **_):
        if not value:
            return value, set()

        single = self.params.get("single", False) or False
        if not isinstance(value, list):
            value = [value]

        files_data = await self.app.services.files.m_get(value)

        if single and files_data:
            return self.app.services.files.normalize(files_data[0]["uri"]), set()

        resp = []
        for i in files_data:
            resp.append(self.app.services.files.normalize(i["uri"]))

        return resp, set()

    async def validate(self, value: Any, **_) -> Tuple[Any, List[ValidationError]]:
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class MediaFileModel(DocumentModel):
    __value__ = MediaFileField()

    is_unique = None
    use_in_grid = UseInGrid(default=True, read_only=True, nested="settings")
    grid_handler = StringField(default="", is_system=True, nested="settings")

    async def validate(self, value: Any, **_) -> Tuple[Any, List[ValidationError]]:
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class VideoAttributeModel(DocumentModel):
    __value__ = VideoField()

    is_unique = None
    use_in_grid = UseInGrid(default=False, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    async def validate(self, value: Any, **_) -> Tuple[Any, List[ValidationError]]:
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class PhotoAttributeModel(DocumentModel):
    __value__ = PhotoField()

    is_unique = None
    use_in_grid = UseInGrid(default=False, nested="settings")

    grid_handler = StringField(default="PhotoGridHandler", is_system=True, nested="settings")
    export_handler = StringField(default="PhotoGridHandler", is_system=True, nested="settings")
    width = IntegerField(negative_value=False, group=FieldGroups.CONDITIONS, nested="settings")
    height = IntegerField(negative_value=False, group=FieldGroups.CONDITIONS, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    async def serialize(self, value, **_):
        if not value:
            return value, set()

        single = self.params.get("single", False) or False
        id = [str(i) for i in value]
        photos = await self.app.services.images.get_gallery(id=id)

        if single and photos:
            photos = photos[0]
            uri = photos["versions"]["original"]["file"]["uri"]
            return self.app.services.files.normalize(uri), set()
        resp = []

        if photos:
            for _v in photos:
                uri = _v["versions"]["original"]["file"]["uri"]
                resp.append(self.app.services.files.normalize(uri))

        return resp, set()

    async def validate(self, value: Any, **_) -> Tuple[Any, List[ValidationError]]:
        value, error = await self._validate(value)
        if not error and not value:
            return None, error

        return value, error


class SearchSettingsField(JSONObjectField):
    _name = "search_settings"

    def __init__(self, *args, **kwargs):
        super(JSONObjectField, self).__init__(*args, **kwargs)

    def repr(self):
        return {
            "default": self.default,
            "kind": self._name,
            "dependence": self.dependence,
        }


class SearchSettingsModel(BaseModel):
    __value__ = SearchSettingsField()

    is_unique = None
    settings_type = SelectField(options_handler="SearchSettings.load", nested="settings", required=True, read_only=True)

    async def validate(self, value: Any, behavior: str = None) -> Tuple[Any, List[ValidationError]]:

        if value and type(value) != dict:
            return value, [ValidationError(
                'Value should be dict',
                data=dict(key=self.key, value=value, code='ValidationError.WrongValueType'))]

        if not value:
            value = {}

        value, validation_errors = await self._validate(value)

        return value, validation_errors


class IntegerRangeModel(BaseEsNormalize, BaseModel):
    class Field(IntegerField):
        _name = "integer_range"

    _type = int
    __value__ = Field()

    is_unique = None
    use_in_filtering = UseInFiltering(default=False, is_system=True)
    use_in_grid = UseInGrid(nested="settings", default=True)
    grid_handler = StringField(default="RangeGridHandler", is_system=True, nested="settings")
    prototype = StringField(is_system=True, default="numeric_range")
    behavior = StringField(is_system=True, default="range", nested="settings")
    negative_value = NegativeValue(default=False, nested="settings")

    async def validate(self, value: dict, **_) -> Tuple[Any, List[ValidationError]]:
        resp_value = {}

        if type(value) is dict:
            min_value = value.get("min")
            max_value = value.get("max")

            if min_value is not None and min_value != '':
                min_value, error_min = await self._validate(min_value)
                if not error_min:
                    resp_value["min"] = min_value
                else:
                    return value, error_min

            if max_value is not None and max_value != '':
                max_value, error_max = await self._validate(max_value)
                if not error_max:
                    resp_value["max"] = max_value
                else:
                    return value, error_max

        if not resp_value:
            return None, None

        return resp_value, None

    @classmethod
    def normalize_es(cls, value):
        if type(value) is dict:
            min_value = value.get("min")
            max_value = value.get("max")

            if (min_value is None or min_value == '') and (max_value is None or max_value == ''):
                return None

            if min_value is not None and min_value != '':
                min_value = cls._type(min_value)
            else:
                min_value = MIN_LONG

            if max_value is not None and max_value != '':
                max_value = cls._type(max_value)
            else:
                max_value = MAX_LONG

            return {"gte": min_value, "lte": max_value}


class DecimalRangeModel(IntegerRangeModel):
    class Field(DecimalField):
        _name = "decimal_range"
    _type = Decimal

    __value__ = Field()

    is_unique = None
    use_in_filtering = UseInFiltering(default=False, is_system=True)
    use_in_grid = UseInGrid(nested="settings", default=True)
    grid_handler = StringField(default="RangeGridHandler", is_system=True, nested="settings")
    prototype = StringField(is_system=True, default="numeric_range")
    behavior = StringField(is_system=True, default="range", nested="settings")


class MetricAttributeModel(BaseModel):
    __value__ = MetricField()

    reference = SelectField(
        required=True,
        read_only=True,
        options_handler="MetricFamily.load",
    )
    is_unique = None
    decimal_value = DecimalValue(nested="settings")
    grid_handler = StringField(default="MetricGridHandler", is_system=True, nested="settings")
    options_handler = StringField(default="MetricOption.load", is_system=True, nested="settings")
    # behavior = Behavior(nested="settings")
    use_in_grid = UseInGrid(nested="settings")
    use_in_filtering = UseInFiltering(default=False)
    read_only = BooleanField(default=False, nested="settings")
    prototype = StringField(is_system=True, default="numeric")

    async def validate(self, value, **_):
        if type(value) is dict:
            _value = value.get("value")
            if isinstance(_value, (int, float, Decimal)):
                return await self._validate(value)

        return None, None

    def simple_normalizer(self, value, unit):
        fm = MetricFamiliesHash[self.params["reference"]]
        return fm.normalize_value(unit, value)

    def normalize_es(self, value):
        if type(value) is dict:
            _value = value.get("value")
            if isinstance(_value, (int, float, Decimal)):
                _val = self.simple_normalizer(value["value"], value["unit"])
                if _val is not None:
                    return round(_val, 5)

        return None


class MetricRangeAttributeModel(MetricAttributeModel):
    class Field(MetricField):
        _name = "metric_range"

    __value__ = Field()

    reference = SelectField(
        required=True,
        read_only=True,
        options_handler="MetricFamily.load",
    )
    is_unique = None
    grid_handler = StringField(default="MetricRangeGridHandler", is_system=True, nested="settings")
    behavior = Behavior(nested="settings", is_system=True, default="range")

    prototype = StringField(is_system=True, default="numeric_range")
    use_in_filtering = UseInFiltering(default=False, is_system=True)

    # negative_value = NegativeValue(default=False)

    async def validate(self, value, **_):
        resp_value = {}
        if type(value) is dict:
            min_d = value.get("min")
            if type(min_d) is dict:
                min_value = min_d.get("value")
                min_unit = min_d.get("unit")

                if min_value is not None and min_value != '' and min_unit:
                    min_value, error_min = await self._validate(min_d)
                    if not error_min:
                        resp_value["min"] = min_value
                    else:
                        return value, error_min

            max_d = value.get("max")
            if type(max_d) is dict:
                max_value = max_d.get("value")
                max_unit = max_d.get("unit")

                if max_value is not None and max_value != '' and max_unit:
                    max_value, error_min = await self._validate(max_d)
                    if not error_min:
                        resp_value["max"] = max_value
                    else:
                        return value, error_min

        return resp_value or None, None

    def normalize_es(self, value):
        _r = {}
        if not value:
            return None

        negative_value = getattr(self, "negative_value", None)

        fm = MetricFamiliesHash[self.params["reference"]]

        _min = value.get("min", {})
        _min_val = fm.normalize_value(_min.get("unit"), _min.get("value"))

        if _min_val is None:
            if negative_value is None or negative_value:
                _min_val = MIN_LONG
            else:
                _min_val = 0
        else:
            if _min_val < MIN_LONG:
                _min_val = MIN_LONG
            elif _min_val > MAX_LONG:
                _min_val = MAX_LONG

        _max = value.get("max", {})
        _max_val = fm.normalize_value(_max.get("unit"), _max.get("value"))

        if _max_val is None:
            _max_val = MAX_LONG
        else:
            if _max_val < MIN_LONG:
                _max_val = MIN_LONG
            elif _max_val > MAX_LONG:
                _max_val = MAX_LONG

        return {"gte": _min_val, "lte": _max_val}


class CategoryAttributeModel(BaseEsNormalize, BaseModel):
    class CategoryField(SelectField):
        _name = "category"

    __value__ = CategoryField()

    is_unique = None

    options_handler = StringField(default="Item.load", is_system=True, nested="settings")
    use_in_grid = UseInGrid(nested="settings", default=True)
    use_in_filtering = UseInFiltering(default=False)
    grid_handler = StringField(default="CategoryGridHandler", is_system=True, nested="settings")
    prototype = StringField(is_system=True, default="string_list")

    def _is_required(self, value):
        return not self.params.get("required") and not value

    def form_normalize(self):
        beh_val = self.params.get("behavior")
        if not beh_val:
            behavior = self._fields.get("behavior")
            if behavior:
                beh_val = behavior.default or None

        self.params["params"] = {
            # "behavior": beh_val,
            "entity": self.params.get("reference")
        }
        self.params["behavior"] = beh_val