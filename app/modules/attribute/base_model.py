from kaiju_model.model import BaseModel

from app.modules.attribute.grid_handlers import (AttributeKindHandler, AttributeGroupHandler, FilterHandler,
                                                 ProjectionHandler)
from app.modules.attribute.validators import attribute_exists_validator, attribute_not_exist_validator
from app.modules.validators import *


class AttributeEditModel(BaseModel, abc.ABC):
    id = KeyField(field_validator=attribute_not_exist_validator)
    key = KeyField(is_system=True)
    kind = SelectField(
        disabled=True, required=True, read_only=True,
        options_handler="AttributeKind.load",
        grid_handler=AttributeKindHandler.__name__,
        nested="settings"
    )

    group = SelectField(
        disabled=False,
        required=False,
        default=False,
        options_handler="AttributeGroup.load",
        field_validator=group_validator,
        grid_handler=AttributeGroupHandler.__name__
    )

    projection = MultiselectField(
        disabled=True,
        required=False,
        default=[],
        read_only=True,
        options_handler="Projection.load",
        field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )

    is_unique = BooleanField(read_only=True, default=False)
    is_default = BooleanField(disabled=True, read_only=True, is_system=True, default=False)
    is_system = BooleanField(disabled=True, read_only=True, is_system=True, default=False)
    # in_completeness = BooleanField(disabled=True, read_only=True, is_system=True)

    entity = StringField(is_system=True)
    grid_handler = StringField(is_system=True)
    uuid = StringField(is_system=True)
    # required = BooleanField(default=False, nested="settings")

    def modify(self, name, field, **_):
        if name == 'group':
            if self.params.get("entity"):
                return {"params": {"entity": self.params.get("entity")}, "is_system": False}

        return {}

class AttributeCreateModel(BaseModel, abc.ABC):
    uuid = StringField(is_system=True)
    id = KeyField(
        # field_validator=attribute_exists_validator
    )

    kind = SelectField(
        disabled=True, required=True, read_only=True,
        options_handler="AttributeKind.load",
        grid_handler=AttributeKindHandler.__name__,
        nested="settings"
    )

    group = SelectField(
        disabled=False,
        required=False,
        default=[],
        options_handler="AttributeGroup.load",
        field_validator=group_validator,
        grid_handler=AttributeGroupHandler.__name__,
    )

    projection = MultiselectField(
        disabled=False,
        required=False,
        default=[],
        options_handler="Projection.load",
        field_validator=group_validator,
        grid_handler=ProjectionHandler.__name__
    )

    is_unique = BooleanField(read_only=True, default=False,
                             # "Теперь это происходит на уровне catalogue"
                             # validator=product_attribute_unique_validator
                             )

    is_default = BooleanField(disabled=True, read_only=True, is_system=True, default=False)
    is_system = BooleanField(disabled=True, read_only=True, is_system=True, default=False)
    # in_completeness = BooleanField(disabled=True, read_only=True, is_system=True)

    grid_handler = StringField(is_system=True, nested="settings")
    entity = StringField(is_system=True)
    # required = BooleanField(default=False, nested="settings")

    def modify(self, name, field, **_):
        if name == 'group':
            if self.params.get("entity"):
                return {"params": {"entity": self.params.get("entity")}, "is_system": False}
        return {}


class AttributeGridModel(BaseModel, abc.ABC):
    uuid = StringField()
    id = StringField()
    attribute_id = StringField(is_system=True)
    label = StringField()
    kind = StringField(grid_handler=AttributeKindHandler.__name__, nested="settings")
    group = StringField()
    projection = MultiselectField(grid_handler=ProjectionHandler.__name__, )
    use_in_filtering = BooleanField(grid_handler=FilterHandler.__name__)
    # behavior = StringField(grid_handler=AttributeBehaviorHandler.__name__)
