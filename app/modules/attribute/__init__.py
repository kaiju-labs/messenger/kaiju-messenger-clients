from .base_model import AttributeGridModel
from .registry import attribute_create_models, attribute_edit_models

__all__ = (
    'attribute_create_models', 'attribute_edit_models', 'AttributeGridModel'
)

# registration of grid handlers
from kaiju_model.grid.constructor import grid_handler_service
from . import grid_handlers

grid_handler_service.register_classes_from_module(grid_handlers)
