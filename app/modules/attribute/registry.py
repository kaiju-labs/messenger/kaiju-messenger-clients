import abc
import inspect
from typing import *

from kaiju_model.model import BaseModel
from kaiju_model.model import ModelMeta
from kaiju_tools.services import AbstractClassRegistry

import app.modules.attribute.kind_models as attributes
import app.modules.attribute.content.kind as content_attributes
from .base_model import AttributeCreateModel, AttributeEditModel


def get_factory():
    class AttributeClassFactory:
        def __init__(self, base_classes: List[Type[BaseModel]] = None):
            if base_classes is None:
                self.base_classes = ModelMeta.BASE_CLASSES
            else:
                self.base_classes = ModelMeta.BASE_CLASSES + base_classes

            self.base_classes = [
                cls for cls in self.base_classes
                if abc.ABC in cls.__bases__ and BaseModel in cls.__bases__
            ]

        def create_classes(self, cls: Type[BaseModel]):
            """Generates attribute classes from base classes and an attribute class."""

            for base_class in self.base_classes:
                name = f'{base_class.__name__}_{cls.__name__}'
                bases = tuple([cls, base_class])
                _cls = type(name, bases, dict(cls.__dict__))
                yield _cls

    return AttributeClassFactory()


_factory = get_factory()


def attrs_factory(attrs):
    for name, cls in tuple(attrs.__dict__.items()):
        if inspect.isclass(cls):
            if issubclass(cls, BaseModel) and abc.ABC not in cls.__bases__:
                for _cls in _factory.create_classes(cls):
                    attrs.__dict__[_cls.__name__] = _cls


# REGISTER ATTRIBUTES:
attrs_factory(attributes)
attrs_factory(content_attributes)
# dsf
del _factory


class AttributeEditKinds(AbstractClassRegistry):
    """Access to attribute classes."""

    base_classes = [AttributeEditModel]

    @staticmethod
    def class_key(obj: AttributeEditModel) -> str:
        return obj.name


attribute_edit_models = AttributeEditKinds()
attribute_edit_models.register_classes_from_module(attributes)

content_attribute_edit_models = AttributeEditKinds()
content_attribute_edit_models.register_classes_from_module(content_attributes)


class AttributeCreateKinds(AbstractClassRegistry):
    """Access to attribute classes."""

    base_classes = [AttributeCreateModel]

    @staticmethod
    def class_key(obj: AttributeCreateModel) -> str:
        return obj.name


attribute_create_models = AttributeCreateKinds()
attribute_create_models.register_classes_from_module(attributes)

content_attribute_create_models = AttributeCreateKinds()
content_attribute_create_models.register_classes_from_module(content_attributes)
