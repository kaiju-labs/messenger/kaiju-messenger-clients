from app.modules.validators import *


async def attribute_exists_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Attribute", "Attribute.is_exist", model_key="entity")
    await _validator(app, key, value, ref)


async def attribute_not_exist_validator(app, key: str, value, ref=None):
    _validator = ServiceValidator("Attribute", "Attribute.is_not_exist", model_key="entity",
                                  is_exists=False)
    await _validator(app, key, value, ref)


async def kind_validator(app, key: str, value, kind, subject="product"):
    attribute = await app.services.EntityClient.call(
        method="Attribute.get",
        params={
            "subject": subject,
            "id": value
        }
    )

    attribute_kind = attribute["kind"]

    if attribute_kind != kind:
        raise ValidationError(f'Attribute kind must be "{kind}", selected {attribute_kind}',
                              data=dict(key=key, value=value, code='ValidationError.InvalidKind'))


async def behavior_validator(app, key: str, value, behavior, subject="product"):
    attribute = await app.services.EntityClient.call(
        method="Attribute.get",
        params={
            "subject": subject,
            "id": value
        }
    )

    attribute_behavior = attribute["settings"]["behavior"]

    if attribute_behavior != behavior:
        raise ValidationError(f'Attribute behavior must be "{behavior}", selected "{attribute_behavior}"',
                              data=dict(key=key, value=value, code='ValidationError.InvalidBehavior'))

