from html.parser import HTMLParser
from io import StringIO

from kaiju_model.grid.base import BaseHandler
from app.modules.grid.common import SimpleMultiSelectGridHandler


async def label_handler(service, values, locale, session=None, _key="id"):
    result = await service.labels([str(i[_key]) for i in values], system_locale=locale, session=session)
    _r = {}

    for i in values:
        _id_l = str(i[_key])

        _val = result.get(_id_l, {}).get(locale, f'[{i[_key]}]')
        _r[i["id"]] = _val or f'[{i[_key]}]'

    return _r


class ExampleHandler(BaseHandler):

    def call(self, values):
        _r = {}

        return _r


class BaseNumberGridHandler(BaseHandler):
    def call(self, values):
        _r = {}

        for i in values:
            behavior = None
            value = i["value"]

            # if not behavior:
            _r[i["id"]] = value
            #     continue
            #
            # if isinstance(value, dict) and behavior == "range":
            #     _min = value.get("min")
            #     _max = value.get("max")
            #     if _min or _max:
            #         _r[i["id"]] = f"""{_min or '..'} - {_max or '..'}"""
            #     else:
            #         _r[i["id"]] = ""
            #
            # elif isinstance(value, list) and behavior == "list":
            #     value = [str(v) for v in value]
            #     _r[i["id"]] = "; ".join(value)
            # else:
            #     _r[i["id"]] = ""

        return _r


class TextGridHandler(BaseHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        class TextHTMLParser(HTMLParser):
            def __init__(self):
                super().__init__()
                self.reset()
                self.strict = False
                self.convert_charrefs = True
                self.text = StringIO()

            def handle_data(self, d):
                self.text.write(d)

            def get_data(self):
                return self.text.getvalue()

        self.html_parser = TextHTMLParser

    def strip_tags(self, html):
        s = self.html_parser()
        s.feed(html)
        return s.get_data()

    def call(self, values):
        _r = {}

        for i in values:
            value = i["value"]

            if not value:
                continue

            if i["model"].params.get("is_rich"):
                value = self.strip_tags(value)

            value = value[:100] if self._meta["short_text"] else value
            _r[i["id"]] = value

        return _r


class AttributeKindHandler(BaseHandler):
    service = "AttributeKind"

    async def call(self, values):
        kind_map = {}

        for i in values:
            ids = kind_map.setdefault(i["value"], [])
            ids.append(i["id"])

        _service = getattr(self.app.services, self.service)

        result = await _service.load(
            system_locale=self._locale,
            id=list(kind_map.keys()),
            per_page=None,
        )
        _r = {}

        for _i in result["data"]:
            for id in kind_map[_i["id"]]:
                _r[id] = _i["label"]
        return _r


class AttributeBehaviorHandler(AttributeKindHandler):
    service = "AttributeBehavior"


class AttributeGroupHandler(BaseHandler):

    async def call(self, values):
        return await label_handler(self.app.services.ProductAttributeGroup, values, self._locale, _key="value")


class ProjectionHandler(SimpleMultiSelectGridHandler):
    _service = "Projection"


class FilterHandler(BaseHandler):
    async def call(self, values):
        entity = self._meta.get('entity')
        if not entity:
            return {}

        resp = {}
        values_for_check = []
        for i in values:
            if i['value']:
                resp[i['id']] = i['model'].params['id']
                values_for_check.append(i['model'].params['id'])
            else:
                resp[i['id']] = None

        if values_for_check:
            _filters = await self.app.services.EntityFilter.check_filters(entity, values_for_check)
            _resp = {}
            for k, v in resp.items():
                if v is not None:
                    _resp[k] = _filters.get(v, None)
                else:
                    _resp[k] = None

            return _resp

        return {}
