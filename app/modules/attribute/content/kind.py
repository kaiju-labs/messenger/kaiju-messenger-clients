from kaiju_model.fields import SelectField, StringField
from ..kind_models import (
    ReferenceMultiselectAttributeModel as BaseReferenceMultiselectAttributeModel,
    ReferenceListAttributeModel as BaseReferenceListAttributeModel,
    ReferenceSelectAttributeModel as BaseReferenceSelectAttributeModel,
    StringModel,
    DecimalModel,
    IntegerModel,
    TextModel,
    JSONObjectModel,
    BooleanModel,
    DateModel,
    DateTimeModel,
    DocumentModel,
    VideoAttributeModel,
    PhotoAttributeModel,
    MediaAttributeModel

)

__all__ = ('ReferenceMultiselectAttributeModel',
           'ReferenceListAttributeModel',
           'ReferenceSelectAttributeModel',
           'StringModel',
           'DecimalModel',
           'IntegerModel',
           'TextModel',
           'JSONObjectModel',
           'BooleanModel',
           'DateModel',
           'DateTimeModel',
           'DocumentModel',
           'VideoAttributeModel',
           'PhotoAttributeModel',
           'MediaAttributeModel'
           )


class ReferenceMultiselectAttributeModel(BaseReferenceMultiselectAttributeModel):
    is_unique = None

    reference = SelectField(
        required=True,
        read_only=True,
        options_handler="ContentEntity.load_by_uuid",
        group='Group.entity'
    )
    options_handler = StringField(default="ContentItem.load_by_uuid", is_system=True, nested="settings")


class ReferenceListAttributeModel(BaseReferenceListAttributeModel):
    is_unique = None

    reference = SelectField(
        required=True,
        read_only=True,
        options_handler="ContentEntity.load_by_uuid",
        group='Group.entity'
    )
    options_handler = StringField(default="ContentItem.load_by_uuid", is_system=True, nested="settings")


class ReferenceSelectAttributeModel(BaseReferenceSelectAttributeModel):
    is_unique = None

    reference = SelectField(
        required=True,
        read_only=True,
        options_handler="ContentEntity.load_by_uuid",
        group='Group.entity'
    )
    options_handler = StringField(default="ContentItem.load_by_uuid", is_system=True, nested="settings")
