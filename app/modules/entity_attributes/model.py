from kaiju_model.model import BaseModel

from app.modules.validators import *


class EntityAttributeCreateModel(BaseModel, abc.ABC):
    entity = SimpleIdField()

    attribute = MultiselectField(required=False)
    group = MultiselectField(required=False)