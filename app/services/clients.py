from __future__ import annotations

import asyncio
import random
import re
import uuid
from typing import Union, List, FrozenSet, Dict, Optional, Collection

try:
    from typing import TypedDict
except ImportError:
    # python 3.6 compatibility
    from typing_extensions import TypedDict

import bcrypt
import sqlalchemy as sa

from kaiju_tools.exceptions import ValidationError, Conflict, NotAuthorized
from kaiju_tools.services import ContextableService
from kaiju_db.services import DatabaseService, SQLService

from .etc import WEAK_PASSWORDS
from ..models.tables import *

__all__ = (
    'ClientService'
)


class ClientService(SQLService, ContextableService):
    class ErrorCodes:

        USER_NOT_FOUND = 'auth.user.not_found'
        USER_EXISTS = 'auth.user.exists'
        USER_AUTH_FAILED = 'auth.user.authentication_failed'
        USER_IDENTICAL_PASSWORDS_SUPPLIED = 'auth.user.identical_passwords_supplied'
        USER_INVALID_EMAIL = 'auth.user.invalid_email'
        USER_INVALID_USERNAME = 'auth.user.invalid_username'
        USER_WEAK_PASSWORD = 'auth.user.weak_password'
        USER_INVALID_PASSWORD = 'auth.user.invalid_password'

    service_name = 'client'
    db_service_class = DatabaseService
    table = clients

    salt_rounds = 13  #: OWASP recommendation
    bad_password_timeout = 0.5  #: timeout in sec if auth failed
    bad_password_timeout_jitter = 0.5  #: timeout jitter in sec if auth failed

    min_password_len = 12  #: OWASP recommendation
    max_password_len = 128  #: OWASP recommendation
    password_regex = re.compile(rf'^(?=.*\d).{{{min_password_len},{max_password_len}}}$')

    min_username_len = 4
    max_username_len = 32
    username_regex = re.compile(rf'^[\w0-9_-]{{{min_username_len},{max_username_len}}}$')

    email_regex = re.compile(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)')

    _username_regex = re.compile(username_regex)
    _password_regex = re.compile(password_regex)
    _email_regex = re.compile(email_regex)

    def __init__(
            self, app, database_service: Union[DatabaseService, str] = None,
            salt_rounds=salt_rounds,
            weak_passwords=None,
            min_password_len=min_password_len,
            max_password_len=max_password_len,
            min_username_len=min_username_len,
            max_username_len=max_username_len,
            bad_password_timeout=bad_password_timeout,
            bad_password_timeout_jitter=bad_password_timeout_jitter,
            logger=None):
        super().__init__(app=app, database_service=database_service, logger=logger)
        if weak_passwords is None:
            self.weak_passwords = WEAK_PASSWORDS
        else:
            self.weak_passwords = frozenset(weak_passwords)
        self.salt_rounds = salt_rounds
        self.bad_password_timeout = bad_password_timeout
        self.bad_password_timeout_jitter = bad_password_timeout_jitter
        self.min_username_len = min_username_len
        self.max_username_len = max_username_len
        self.min_password_len = min_password_len
        self.max_password_len = max_password_len

    async def exists(self, id) -> Union[bool, frozenset]:
        if isinstance(id, Collection) and not type(id) is str:
            sql = self.table.select().with_only_columns([self.table.c.id]).where(
                self.table.c.id.in_(id)
            )
            rows = await self._db.fetch(sql)
            return frozenset((row['id'] for row in rows))
        else:
            sql = self.table.select().with_only_columns([self.table.c.id]).where(
                self.table.c.id == id
            )
            user = await self._db.fetchrow(sql)
            return user is not None

    async def register(
            self, username: str, email: str, password: str,
            columns='*', **settings):
        """Adds a new user. Used by administrators or user managers."""

        self.validate_username(username)
        self.validate_email(email)
        password = self.validate_password(password)

        sql = self.table.select().where(
            sa.or_(
                self.table.c.username == username,
                self.table.c.email == email
            )
        )

        data = await self._db.fetchrow(sql)

        if data is not None:
            raise Conflict(
                'User or e-mail address is already registered.', key=username,
                code=self.ErrorCodes.USER_EXISTS)

        salt, password = self._hash_password(username, password)
        settings['username'] = username
        settings['email'] = email
        settings['password'] = password
        settings['salt'] = salt
        settings['id'] = uuid.uuid4()
        sql = self.table.insert().values(settings)
        await self._db.execute(sql)
        return await self._get_user(settings['id'], columns)

    async def get_user_info_by_user_id(self, user_id):
        data = await self.get(user_id, columns=['id',
                                                'email',
                                                'created',
                                                'settings',
                                                'private_info',
                                                'public_info'])
        return {
            **data
        }

    async def get_user_id_by_username(self, email_list: str):
        sql = self.table.select().with_only_columns([
            self.table.c.id,
            self.table.c.username
        ]).where(self.table.c.username.in_(email_list))
        clients_id = await self._db.fetch(sql)
        result = []
        for client in clients_id:
            client = dict(client)
            result.append(client['id'])
        if len(result) == 0:
            return {"data": {}, "status": "User not found"}
        return {"data": result, "status": "OK"}

    def get_user_info(self, user_id):
        return self.get_user_info_by_user_id(user_id)

    async def get_profile(self, user_id):
        data = await self.get(user_id, columns=['settings'])
        return {
            'id': user_id,
            'settings': data['settings']
        }

    async def get_user_public_info_by_id(self, user_id):
        if type(user_id) is not list:
            user_id = [user_id]
        sql = sa.select([
            self.table.c.id,
            self.table.c.email,
            self.table.c.public_info
        ]).where(self.table.c.id.in_(user_id))
        user = await self._db.fetch(sql)
        if user is None:
            return {"data": {}, "status": "Users not found"}
        return {"data": user, "status": "OK"}

    async def update_profile_settings(self, user_id, settings: dict):
        data = await self.get(user_id, columns=['settings'])
        meta = data['settings']
        for key, value in settings.items():
            if value is None:
                if key in meta:
                    del meta[key]
            else:
                meta[key] = value
        sql = self.table.update().values(settings=meta).where(self.table.c.id == user_id)
        await self._db.execute(sql)
        return {
            'id': user_id,
            'settings': meta
        }

    async def update_profile_private(self, user_id, private_info: dict):
        data = await self.get(user_id, columns=['private_info'])
        meta = data['private_info']
        for key, value in private_info.items():
            if value is None:
                if key in meta:
                    del meta[key]
            else:
                meta[key] = value
        sql = self.table.update().values(private_info=meta).where(self.table.c.id == user_id)
        await self._db.execute(sql)
        return {
            'id': user_id,
            'private_info': meta
        }

    async def update_profile_public(self, user_id, public_info: dict):
        data = await self.get(user_id, columns=['public_info'])
        meta = data['public_info']
        for key, value in public_info.items():
            if value is None:
                if key in meta:
                    del meta[key]
            else:
                meta[key] = value
        sql = self.table.update().values(public_info=meta).where(self.table.c.id == user_id)
        await self._db.execute(sql)
        return {
            'id': user_id,
            'public_info': meta
        }

    async def auth(self, username: str, password: str, columns='*'):
        sql = self.table.select().with_only_columns([
            self.table.c.id,
            self.table.c.password,
            self.table.c.salt
        ]).where(
                self.table.c.username == username
        )
        user = await self._db.fetchrow(sql)

        if user is None:
            raise NotAuthorized(
                'User authentication failed.',
                code=self.ErrorCodes.USER_AUTH_FAILED)

        if not self._check_password(username, password, user['salt'], user['password']):
            raise NotAuthorized(
                'User authentication failed.',
                code=self.ErrorCodes.USER_AUTH_FAILED)

        return await self._get_user(user['id'], columns)

    async def change_password(self, username: str, password: str, new_password: str):
        if password == new_password:
            raise ValidationError(
                'Old password matches the new one.',
                code=self.ErrorCodes.USER_IDENTICAL_PASSWORDS_SUPPLIED)

        new_password = self.validate_password(new_password)
        user = await self.auth(username=username, password=password)
        await self.set_password(user['id'], username, new_password)
        return True

    async def set_password(self, id, username: str, password: str):
        salt, password = self._hash_password(username, password)
        sql = self.table.update().where(
            sa.and_(
                self.table.c.id == id,
                self.table.c.username == username
            )
        ).values(
            password=password,
            salt=salt
        )
        await self._db.execute(sql)

    def validate_email(self, email: str):
        if not self._email_regex.fullmatch(email):
            raise ValidationError(
                f'Enter a valid e-mail address.',
                key='email', value=None,
                code=self.ErrorCodes.USER_INVALID_EMAIL)

    def validate_username(self, username: str):
        return True
        # if not self._username_regex.fullmatch(username):
        #     raise ValidationError(
        #         f'Username must be from {self.min_username_len} up to'
        #         f' {self.max_username_len} characters,'
        #         f' only letters, numbers and _ . - are allowed.',
        #         key='username', value=None,
        #         min_characters=self.min_username_len,
        #         max_characters=self.max_username_len,
        #         code=self.ErrorCodes.USER_INVALID_USERNAME)

    def validate_password(self, password: str):
        password = password.strip(' \n\t\r')
        if password in self.weak_passwords:
            raise ValidationError(
                f'Password is too weak or compromised.',
                key='password', value=None,
                code=self.ErrorCodes.USER_WEAK_PASSWORD)

        elif not self._password_regex.fullmatch(password):
            raise ValidationError(
                f'Password must contain {self.min_password_len} up to'
                f' {self.max_password_len} characters at least one of them'
                f' must be a digit.',
                key='password', value=None,
                min_characters=self.min_password_len,
                max_characters=self.max_password_len,
                code=self.ErrorCodes.USER_INVALID_PASSWORD)

        return password

    def _hash_password(self, username, password, salt=None) -> (bytes, bytes):
        if salt is None:
            salt = bcrypt.gensalt(self.salt_rounds)
        hashed = bcrypt.hashpw(password.encode('utf-8'), salt)
        if not self._check_password(username, password, salt, hashed):
            raise RuntimeError('Internal error.')
        return salt, hashed

    def _check_password(self, username, password, salt, hashed) -> bool:
        result = bcrypt.checkpw(password.encode('utf-8'), hashed)
        return result

    def _get_timeout(self):
        return random.random() * self.bad_password_timeout_jitter + self.bad_password_timeout

    def process_update_data(self, password=None, salt=None, created=None, id=None, **data):
        return data

    async def _get_user(self, id, columns='*'):
        user = await self.get(id, columns=columns)
        return {
            **user
        }
