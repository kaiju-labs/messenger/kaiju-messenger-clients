from typing import Optional, TypedDict, Union

from kaiju_tools.exceptions import Conflict, ValidationError
from kaiju_tools.services import ContextableService
from kaiju_tools.rpc import AbstractRPCCompatible

from .clients import ClientService
from kaiju_auth import JWTService

__all__ = (
    'HTTPClientLoginService', 'ErrorCodes'
)


class ErrorCodes:
    AUTH_METHOD_DISABLED = 'auth.auth_method_disabled'
    NOT_CORRECT_DATA_TYPE = 'auth.update.incorrect_data_type'


class HTTPClientLoginService(ContextableService, AbstractRPCCompatible):
    class TokenResponse(TypedDict):
        access: str
        refresh: str
        exp: int
        refresh_exp: int

    service_name = 'Clients'
    token_service_class = JWTService
    user_service_class = ClientService
    ErrorCodes = ErrorCodes

    def __init__(
            self, app,
            user_service: Union[str, user_service_class] = None,
            token_service: Union[str, token_service_class] = None,
            logger=None):

        super().__init__(app=app, logger=logger)
        self._user_service_name = user_service
        self._token_service_name = token_service
        self._client_service = None
        self._token_service = None

    @property
    def routes(self):
        routes = {
            'register': self.register,
            'login': self.jwt_get,
            'change_password': self.change_password,
            'jwt_refresh': self.jwt_refresh,
            'update_profile': self.update_profile,
            'user_info': self.get_user,
            'user_id_by_email': self.get_user_id_by_email,
            'user_public_info': self.get_user_public_by_user_id,
            'search_clients': self.search_clients,
        }
        return routes

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    async def init(self):
        self._client_service = self.discover_service(
            self._user_service_name, cls=self.user_service_class, required=False)
        self._token_service = self.discover_service(
            self._token_service_name, cls=self.token_service_class, required=False)

    async def register(self, email: str, password: str):
        self._check_client_service_is_available()
        await self._client_service.register(email, email, password)
        return await self.jwt_get(email, password)

    async def jwt_get(self, email: str, password: str) -> TokenResponse:
        self._check_client_service_is_available()
        self._check_token_service_is_available()
        user = await self._client_service.auth(username=email, password=password)
        return await self._create_tokens(user)

    async def _create_tokens(self, user_info: dict, ttl=None):
        data = {
            'user_id': user_info['id']
        }
        access, refresh = await self._token_service.generate_token_pair(ttl=ttl, **data)
        return {
            'access': access.serialize(compact=True),
            'refresh': refresh.serialize(compact=True),
            'exp': access.claims_data['exp'],
            'refresh_exp': refresh.claims_data['exp']
        }

    async def jwt_refresh(self, refresh: str) -> TokenResponse:
        self._check_token_service_is_available()
        access, refresh = await self._token_service.refresh_token(refresh)
        return {
            'access': access.serialize(compact=True),
            'refresh': refresh.serialize(compact=True),
            'exp': access.claims_data['exp'],
            'refresh_exp': refresh.claims_data['exp']
        }

    async def update_profile(self, access_token: str, data: dict, data_type: str):
        token = await self._token_service.verify_token(access_token)
        user_id = token.claims_data['user_id']
        function = {
            "settings": self._client_service.update_profile_settings,
            "public": self._client_service.update_profile_public,
            "private": self._client_service.update_profile_private
        }
        if data_type in function:
            return await function[data_type](user_id, data)
        else:
            raise ValidationError("Not correct data type", code=self.ErrorCodes.NOT_CORRECT_DATA_TYPE)

    async def search_clients(self, access_token: str, email: str, limit: int, offset: int):
        token = await self._token_service.verify_token(access_token)
        user_id = token.claims_data['user_id']
        result = await self._client_service.list(conditions={'email': {'like': email + "%"}},
                                                 limit=limit,
                                                 offset=offset,
                                                 columns=['id', 'created', 'email', 'public_info'])
        return result

    async def change_password(self, access_token: str, password: str, new_password: str):
        token = await self._token_service.verify_token(access_token)
        user_id = token.claims_data['user_id']
        user = await self._client_service.get_user_info_by_user_id(user_id=user_id)
        username = user["username"]
        return await self._client_service.change_password(username, password, new_password)

    async def get_user(self, access_token: str):
        token = await self._token_service.verify_token(access_token)
        user_id = token.claims_data['user_id']
        return await self._client_service.get_user_info_by_user_id(user_id=user_id)

    async def get_user_id_by_email(self, email: str):
        if type(email) is str:
            temp = await self._client_service.get_user_id_by_username(email_list=[email])
            if temp['status'] == "OK":
                return {"data": {"user_id": temp['data'][0]}, "status": "OK"}
            else:
                return {"data": {}, "status": temp['status']}
        if type(email) is list:
            temp = await self._client_service.get_user_id_by_username(email_list=email)
            if temp['status'] == "OK":
                return {"data": {"user_id": temp['data']}, "status": "OK"}
            else:
                return {"data": {}, "status": temp['status']}
        # return await self._client_service.get_user_id_by_username(username=email)

    async def get_user_public_by_user_id(self, user_id):
        return await self._client_service.get_user_public_info_by_id(user_id=user_id)

    @property
    def token_backend_enabled(self) -> bool:
        return self._token_service_name is not False

    def _check_token_service_is_available(self):
        if not self._token_service:
            raise Conflict(
                'Token service is disabled.',
                service=self.service_name,
                code=self.ErrorCodes.AUTH_METHOD_DISABLED)

    def _check_client_service_is_available(self):
        if not self._client_service:
            raise Conflict(
                'User service is disabled.',
                service=self.service_name,
                code=self.ErrorCodes.AUTH_METHOD_DISABLED)
